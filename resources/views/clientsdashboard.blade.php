<style type="text/css">
    
.btn-primario {
  color: white;
  background-color: {{$client->primarycolor}};
}


.btn-secundario {

color: white;
  background-color: {{$client->secondarycolor}};
}

</style>

@extends('layouts.dash')

@section('content')


<div class="main-content" >
	<div class="section__content section__content--p30">
		<div class="container-fluid">  
			<div class="au-card recent-report">   
                <div class="row">
  					<div class="col-sm-12" align="center">               
                       <img src="{{$client->logoURL}}" class="img-fluid">		
						<br>
						<h2 class="title-1">{{$client->description}}</h2>
 					 </div>
                </div>
                <br>
            <hr class="pleca" style="width: 100%; border-color: {{$client->primarycolor}};">
        </div>
            <div class="au-card recent-report">   
                <div class="row">
                    <div class="col-sm-12">
                    <h2 class="title-1">Marcas</h2>
            <hr class="pleca" style="border-color: {{$client->primarycolor}};">
<!-- inicia tabla -->
                    <div class="table-responsive table--no-card m-b-40">
                        <table class="table table-earning">
                            <thead>
                                <tr>
                                    <th style="background-color: {{$client->primarycolor}};">Logo </th>
                                    <th style="background-color: {{$client->primarycolor}};">Nombre </th>
                                </tr>
                            </thead>
                            <tbody>
                                            <tr>
                                            <tr>
                                        @forelse ($products as $product)
                                              @if($product->client_id == $client->id)
                                              @endif
                                               <td align="center">                                                   
<img src="{{$product->logoURL}}" style="max-width: 150px; height: auto;">

                                               </td>
                                                <td style=" vertical-align: middle;">{{ $product->name }}</td>
                                              
                                             
                                                
                                               </td>
                                            </tr>            
                                        @empty
                                        	No hay datos para mostrar
                                        @endforelse                
                                        </tbody>
                                    </table>
                                </div>
<!-- termina tabla -->


			<h2 class="title-1">Campañas</h2>
			<hr class="pleca" style="border-color: {{$client->primarycolor}};">
            <br>
	<!-- inicia tabla -->

                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-earning">
                                        <thead>
                                            <tr>
                                                
                                                <th style="background-color: {{$client->secondarycolor}};">Nombre  </th>
     
                                         
                                            </tr>
                                        </thead>
  
                                        <tbody>
                                            <tr>
                                        @forelse ($routes as $route)
                                               @if($route)
                                                <td>{{ $route->name }}</td>

                                                	
                                                </td>
                                                @endif
                                                
                                               </td>
                                            </tr>            
                                        @empty

                                        @endforelse                
                                        </tbody>
                                    </table>
                                </div>
<!-- termina tabla -->

				<h2 class="title-1">Eventos</h2>
			<hr class="pleca" style="border-color: {{$client->primarycolor}};">

            <!-- mapa -->


<br>
				<!-- inicia tabla -->

                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-earning">
                                        <thead>
                                            <tr>
                                                
                                                <th style="background-color: {{$client->primarycolor}}" >Nombre  </th>
                                                 <th style="background-color: {{$client->primarycolor}}" >Fecha</th>
                                                 <th style="background-color: {{$client->primarycolor}}" >Actividades Previas</th>
                                                 <th style="background-color: {{$client->primarycolor}}" >Datos</th>
                                         
                                            </tr>
                                        </thead>
  
                                        <tbody>
                                            <tr>
                                        @forelse ($checkpoints as $checkpoint)
                                              
                                                <td style="vertical-align: middle;">{{ $checkpoint->name }}
                                                	<br>
                                                	<small>{{$checkpoint->fullAddress}}</small>
                                                </td>
                                                <td style="vertical-align: middle;">
     												{{  \Carbon\Carbon::parse($checkpoint->arrivalDate)->format('d/m/Y') }}
                                                </td>

                                                                                                                             <td>

                                                    <a href="#" class="btn btn-dark btn-md"> Montaje</a>
                                                     <a href="#" class="btn btn-dark btn-md"> Actividades</a>
                                                </td>

                                                                                     <td>

                                                    <a href="{{route('checkpoints.show', $checkpoint->id)}}" class="btn btn-secundario btn-block"> ver datos</a>
                                                </td>
                                               
                                                
                                               </td>
                                            </tr>            
                                        @empty

                                        @endforelse                
                                        </tbody>
                                    </table>
                                </div>
<!-- termina tabla -->






@endsection
