<!DOCTYPE html>
<html lang="es">
<head>




    <!-- Load jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

<!-- Load ScrollTo -->
<script src="http://flesler-plugins.googlecode.com/files/jquery.scrollTo-1.4.2-min.js"></script>

<!-- Load LocalScroll -->
<script src="http://flesler-plugins.googlecode.com/files/jquery.localscroll-1.2.7-min.js"></script>

<script type = "text/javascript">
 $(document).ready(function()
    {
        // Scroll the whole document
        $('#menuBox').localScroll({
           target:'#content'
        });
    });
</script>

    <!-- Meta Tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="descripción del sitio">
    <meta name="author" content="Oliver Montor">
    <meta name="keywords" content="keywords del sitio">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @auth
        <meta name="user-id" content="{{ Auth::user()->id }}">
        <meta name="user-role" content="{{ Auth::user()->roles[0]->name }}">
    @endauth

    <meta name="user-id" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'The Brix') }}</title>



    <!-- Fonts -->
    <script src="https://kit.fontawesome.com/9724818a19.js"></script>
    <!-- Styles -->



    <!-- Fontfaces CSS-->
    <link href="/css/font-face2.css" rel="stylesheet" media="all">
    <link href="/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/css?family=Anton&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS-->
    <link href="/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="/css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="{{route('home')}}">
                            <img src="/images/logohor.png" alt="Logo" width="30%" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li>
                            <a href="{{ url('/dashboard/view') }}">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>Usuarios</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{route('admin.users')}}">Ver Todos</a>
                                    <a href=""> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                                                <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-building"></i>Clientes</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                   <a href="{{route('admin.clients')}}">Ver Todos</a>
                                    <a href=""> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-briefcase"></i>Marcas</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{route('admin.products')}}">Ver Todos</a>
                                    <a href=""> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-calendar"></i>Eventos</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{route('admin.checkpoints')}}">Ver Todos</a>
                                    <a href=""> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                        <li class="has-sub">
                            <a class="js-arrow" href="#">
                                <i class="fas fa-map-marker-alt"></i>Venues</a>
                            <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                                <li>
                                    <a href="{{route('admin.venues')}}">Ver Todos</a>
                                    <a href=""> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="/images/logohor.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">

                        @role('admin')
                        <li>
                            <a href="{{ url('/dashboard/view') }}">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>Usuarios</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.users')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                                                <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-building"></i>Clientes</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.clients')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                             <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-briefcase"></i>Marcas</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.products')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>



                         <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-calendar"></i>Eventos</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.checkpoints')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-map-marker-alt"></i>Venues</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.venues')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li> 
                            <li>
                           
                             <a href="map.html" class="btn btn-block btn-dark" style="color: white;"> + Cargar Datos</a>
                            </li>   
                        </li>
                           @endrole
                           @role('client')
                           Menú Cliente
                                                   <li>
                            <a href="{{ url('/dashboard/view') }}">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>Usuarios</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.users')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>
                           @endrole

@role('employee')


                    
                          <li>
                            <a href="/home">
                                <i class="fas fa-home"></i>Regresar al Inicio</a>
                            </li>

                            
                            <li>
                            <a href="#InfoGral">
                                <i class="fas fa-building"></i>Información General</a>
                            </li>


   
                            <li>
                            <a href="#Estadisticos">
                                <i class="fas fa-line-chart"></i>Datos Estadísticos</a>
                            </li>

   
                            <li>
                            <a href="#Plazas">
                                <i class="fas fa-map-marker-alt"></i>Geolocalización</a>
                            </li>

   
                            <li>
                            <a href="#Analytics">
                                <i class="fas fa-signal"></i>Estadísticas Web</a>
                            </li>

   
                            <li>
                            <a href="#Video">
                                <i class="fas fa-video-camera"></i>Video</a>
                            </li>

   
                            <li>
                            <a href="#Galeria">
                                <i class="fas fa-file-excel"></i>Galería</a>
                            </li>

                            
                            
                           @endrole

                           @role('operator')
                           Menú Operador
                                                   <li>
                            <a href="{{ url('/dashboard/view') }}">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>Usuarios</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.users')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>
                           @endrole

                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
           <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">

                    
                       
                        
                        <div class="header-wrap">
                                    <!-- PARA HACER ESPACIO EN LO QUE SE HACE LA FUNCIONALIDAD DE AQUÍ-->
                               <p>  </p>
                         <p>  </p> 
                          <p>  </p> 
                           <p>  </p> 
                            <p> </p>   
                            <!--<form class="form-header" action="" method="POST">
                                <input class="au-input au-input--xl" type="text" name="search" placeholder="Buscar cliente, marca, evento o venue" />
                                <button class="au-btn--submit" type="submit">
                                    <i class="zmdi zmdi-search"></i>
                                </button>
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-comment-more"></i>
                                        <span class="quantity">1</span>
                                        <div class="mess-dropdown js-dropdown">
                                            <div class="mess__title">
                                                <p>You have 2 news message</p>
                                            </div>
                                            <div class="mess__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-06.jpg" alt="Michelle Moreno" />
                                                </div>
                                                <div class="content">
                                                    <h6>Michelle Moreno</h6>
                                                    <p>Have sent a photo</p>
                                                    <span class="time">3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="mess__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-04.jpg" alt="Diane Myers" />
                                                </div>
                                                <div class="content">
                                                    <h6>Diane Myers</h6>
                                                    <p>You are now connected on message</p>
                                                    <span class="time">Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="mess__footer">
                                                <a href="#">View all messages</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-email"></i>
                                        <span class="quantity">1</span>
                                        <div class="email-dropdown js-dropdown">
                                            <div class="email__title">
                                                <p>You have 3 New Emails</p>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-06.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, 3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-05.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-04.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, April 12,,2018</span>
                                                </div>
                                            </div>
                                            <div class="email__footer">
                                                <a href="#">See all emails</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a email notification</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Your account has been blocked</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a new file</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                                      
                                                            <img src="{{Auth::user()->avatar}}">
        
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">{{ Auth::user()->name }}   </a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                  </a>
                                                      <img src="{{Auth::user()->avatar}}">
                                             
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#">{{ Auth::user()->name }}   </a>
                                                    </h5>
                                                    <span class="email">{{ Auth::user()->email }}   </span>
                                                </div>
                                            </div>
                                            <!-- 
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Cuenta</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Cambiar Password</a>
                                                </div>
                                            </div>
                                            -->
                                            <div class="account-dropdown__footer">
                                                <a href="{{ route('logout')}}" onclick="event.preventDefault();
                                                                             document.getElementById('logout-form').submit();">
                                                    <i class="zmdi zmdi-power"></i>Salir</a>
                                                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                            </form>

                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
                                

                       
  
            </header>

            @if (session()->has('success'))

            <div class="container">              
                 <div class="alert alert-success">{{ session('success')}}</div>
            </div>
            @endif


            @if (session()->has('warning'))

            <div class="container">              
                 <div class="alert alert-warning">{{ session('warning')}}</div>
            </div>
            @endif

                <div id="app">
    

        <main class="py-4">
            @yield('content')
        </main>

  
                               
                                <div class="copyright">
                                    <p>    {{config('app.name')}} | Copyright  {{date('Y')}}</small></p>
                                </div>
                    

</div>
  <!-- Jquery JS-->
    <script src="/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="/vendor/slick/slick.min.js">
    </script>
    <script src="/vendor/wow/wow.min.js"></script>
    <script src="/vendor/animsition/animsition.min.js"></script>
    <script src="/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="/js/main.js"></script>

@yield('scripts')


</body>


</html>





