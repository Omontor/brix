@extends('layouts.dash')

@section('content')
<div class="main-content">
   <div class="container-fluid">  
	 <div class="au-card recent-report">  
   		 <div class="container">
       		 <div class="row justify-content-center">
       		 	<div class="col-lg-12">
				@role('admin')
				<h2 class="title-1">Usuarios</h2>
				<hr class="pleca">
					   
				<br>					                
				<div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                 <th>Avatar  </th>
                                                <th>Nombre  </th>
                                                <th>Contacto  </th>
                                                <th>Rol  </th>
                                                
                                            </tr>
                                        </thead>
  
                                        <tbody>
                                            <tr>
                                        @forelse ($users as $user)
                                                
                                            <td style="align-content: center;">
                                              <center>
                                            <img src="{{$user->avatar}} " class="img-fluid" style="width: 100px; border-radius: 50px">
                                            </center>            
                                               </td>
                                                <td style="align-content: center; vertical-align: middle;">
                                                  <h4>{{$user->name}} </h4>
										                           					
                                               </td>
                                               <td style="align-content: center; vertical-align: middle;">
                                               <h4>{{$user->email}}     </h4>    
                                               <br>
                                               {{$user->contact}}     
                                               </td>
                                                <td style="align-content: center; vertical-align: middle;">
                                                 
                                               <h4>{{$user->roles[0]['name']}} </h4>               
                                               </td>


                                            </tr>            
                                        @empty

                                        @endforelse                
                                        </tbody>
                                    </table>
                                </div>
                           
                             </div>
                             </div>   
                        </div>               			             
				@endrole
					        </div>
					    </div>
					</div>


@endsection
