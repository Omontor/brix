<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="/js/highstock.js"></script>
<script src="/js/highmaps.js"></script>
@extends('layouts.dash')

@section('content')
            <!-- Contenido del dashboard-->
            <div class="main-content" >
                <div class="section__content section__content--p30">
                    <div class="container-fluid">  
                          <div class="au-card recent-report">                
                                <h2 class="title-1">Datos Generales</h2>
                                <hr class="pleca">
                            <div class="row m-t-25">

                                <div class="col-sm-6 col-lg-4">
                                    <div class="icon">
                                     <center>
                                                <img src="/images/icons/cliente.png" class="img-fluid">
                                                </center>
                                            </div>
                                <div class="overview-item overview-item--negro">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="text">
                                                <h2>{{$clients}}</h2>
                                                <h1 style="color: white">Clientes</h1>
                                            </div>
                                        </div>
                                        <div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <div class="icon">
                                <center>
                                 <img src="/images/icons/chart.png" class="img-fluid">
                                </center>
                                </div>
                                <div class="overview-item overview-item--negro">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="text">
                                                <h2>{{$products}}</h2>
                                                 <h1 style="color: white">Campañas</h1>
                                            </div>
                                        </div>
                                        <div>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="col-sm-6 col-lg-4">
                              <div class="icon">
                                <center>
                                                <img src="/images/icons/target.png" class="img-fluid">
                                                </center>
                                            </div>
                                <div class="overview-item overview-item--negro">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="text">
                                                <h2>{{$routes}}</h2>
                                                 <h1 style="color: white">Eventos</h1>
                                            </div>
                                        </div>
                                        <div>
                                            <br>
                                           
                                        </div>

                                    </div>

                                </div>
                        </div>  
                                 <div class="col-sm-6 col-lg-3">
                                       <div class="icon">
                                        <img src="/images/icons/usuarios.png">
                                        </div>
                                    <div class="overview-item overview-item--amarillo">
                                        <div class="overview__inner">
                                            <div class="overview-box clearfix">
                                             
                                                <div class="text">
                                                    <h2>{{$employees + $operators + $clients}}</h2>
                                                     <h1 style="color: white">Usuarios</h1>
                                                    </div>
                                                 <div>
                                                     <br>                                           
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                     </div>
                                    <div class="col-sm-6 col-lg-9">
                                        <figure class="highcharts-figure">
                                        <div id="containerA"></div>
                                        <script>
                                           Highcharts.chart('containerA', {
    chart: {
        type: 'areaspline'
    },

    colors: ['#e6e600', '#993d00', '#000000', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'],

    title: {
        text: 'Asistencia'
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
    },
    xAxis: {
        categories: [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ],
        
    },
    yAxis: {
        title: {
            text: 'Fruit units'
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' units'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.5
        }
    },
    series: [{
        name: 'Asistencia',
        data: [3, 4, 3, 5, 4, 10, 12]
    }, {
        name: 'Retardo',
        data: [1, 3, 4, 3, 3, 5, 4]
    },
    {
        name: 'Falta',
        data: [2, 1, 3, 6, 1, 0, 0]
    }

    ]
});

                                        </script>
                                        </figure>
                                     </div>                           
                    </div>
                    <!-- Termina Row cabecera-->

                    
                       <h2 class="title-1">Clientes y Marcas</h2>
                                <hr class="pleca">
                                <br>
                                <div class="row">

                            <div class="col-lg-6">
                                <div class="au-card au-card--no-shadow au-card--no-pad m-b-40 shadow-sm">
                                    <div class="au-card-title" style="background-image:url('/images/bg-title-01.jpg');">
                                        <div class="bg-overlay" style="background-color: rgba(0,0,0,.8);"></div>
                                        <h3>
                                            <i class="zmdi zmdi-account-calendar"></i>Últimos Clientes Registrados</h3>
        
                                    </div>
                                    <div class="au-task js-list-load">
                                        <div >
                                            <div class="au-task__item au-task__item--warning">
                                                <div class="au-task__item-inner">
                                                    
                                                    @forelse ($clientstotal as $clients)
                                                     <h5 class="task my-2" >

                                                     <a href="#">{{$clients->created_at->diffForHumans()}}</a>
                                                    </h5>
                                                    <span class="time" style="font-weight: 400pt; font-size: 15pt;">{{$clients->name}}</span>  
                                                    <br>                                             
                                                    <a class="btn btn-dark btn-block" style="color: white; float: right;" href="{{route('clients.show', $clients->id)}}"> Ver Dashboard</a>
                                                    <br>
                                                    <br>
                                                    <hr>

                                                     @empty
                                                      No hay datos para mostrar
                                                     @endforelse
   
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                 <div class="col-lg-6">
                                <div class="au-card au-card--no-shadow au-card--no-pad m-b-40 shadow-sm">
                                    <div class="au-card-title" style="background-image:url('/images/bg-title-01.jpg');">
                                        <div class="bg-overlay" style="background-color: rgba(0,0,0,.8);"></div>
                                        <h3>
                                            <i class="zmdi zmdi-account-calendar"></i>Últimas Marcas Registradas</h3>
        
                                    </div>
                                    <div class="au-task js-list-load">
                                        <div>
                                            <div class="au-task__item au-task__item--warning">
                                                <div class="au-task__item-inner">
                                                    @forelse ($productstotal as $products)
                                                     <h5 class="task"  style="font-size: 15pt;">
                                                   {{$products->name}}
                                                     
                                                    </h5>
                                                    
                                                    <span class="time">{{$clientescompletos[$products->client_id - 1]->name}}</span>
                                                 <br>                   
                                                    <a class="btn btn-dark btn-block" style="color: white; float: right;" href="{{route('clients.show', $clients->id)}}"> Ver Dashboard</a>
                                                    
                                                    <br>
                                                    <br>
                                                    <hr>
                                                    
                                                     @empty
                                                      No hay datos para mostrar
                                                     @endforelse
   
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                           </div>              
             </div> 

        <div class="row">
             <div class="col-md-12">
                     <div class="au-card recent-report">
                        <h2 class="title-1">Sedes</h2>   
                        <hr class="pleca">
                        <br>   

                        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var markers = [

                @forelse($venues as $venue)

    {     
        "title": '{{$venue->name}}}',
        "lat": '{{$venue->lat}}',
        "lng": '{{$venue->lng}}',
        "description": '{{$venue->name}}'
    },
    @empty
    @endforelse
    
    ];
    window.onload = function () {
        LoadMap();
    }
    function LoadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            // zoom: 8, //Not required.
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
 
        //Create LatLngBounds object.
        var latlngbounds = new google.maps.LatLngBounds();
 
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
 
            //Extend each marker's position in LatLngBounds object.
            latlngbounds.extend(marker.position);
        }
 
        //Get the boundaries of the Map.
        var bounds = new google.maps.LatLngBounds();
 
        //Center map and adjust Zoom based on the position of all markers.
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
    }
</script>
<div id="dvMap" style="width: 100%; height: 400px">
</div>


<script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-VZUr01EdXUPZfUcj_UilKvo1DjmfGG0&callback=initMap">
                        </script>
                        <br>
                        <br>

                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                
                                                <th>Nombre</th>
                                         
                                            </tr>
                                        </thead>
  
                                        <tbody>
                                            <tr>
                                        @forelse ($venues as $venue)
                                               
                                                <td>{{ $venue->name }}</td>

                                                
                                               </td>
                                            </tr>            
                                        @empty

                                        @endforelse                
                                        </tbody>
                                    </table>
                                </div>
                           
                             </div>
                             </div>   
                        </div>



                        <div class="row">
                            <div class="col-lg-12">
                                <div class="au-card recent-report">
                                <h2 class="title-1">Últimos Usuarios Registrados</h2>
                                <hr class="pleca">
                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>

                                                <th>Nombre  </th>
                                                <th>Teléfono</th>
                                                <th>Activo</th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                            <tr>
                                        @forelse ($usertotals as $usertotal)


                                                <td>{{ $usertotal->name }}
                                                <br>
                                                <small>{{ $usertotal->email }}</small>
                                                </td>
                                                <td>{{ $usertotal->contact }}</td>
                                            
                                                <td>@if( $usertotal->isActive  == 1)
                                                    <span style="color: green;"> Activo</span>
                                                    @else
                                                    <span style="color: red;"> Sin Activar</span>
                                                    @endif

                                                </td>
                                            </tr>            
                                        @empty

                                        @endforelse                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            </div>

                                        
                                </div>                      
                            </div>
                           

                    
                        </div>

                        </div>
                    </div>
                    
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->


@endsection
    

            
    


