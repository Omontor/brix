@extends('layouts.dash')

@section('content')


<div class="main-content">
    <div class="container-fluid">  
     <div class="au-card recent-report">  
         <div class="container">
             <div class="row justify-content-center">
			                <div class="col-lg-12">
			 	@role('admin')
			         

			            <new-checkpoint></new-checkpoint>
			        @endrole
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-VZUr01EdXUPZfUcj_UilKvo1DjmfGG0&libraries=places"></script> 
@endsection
 @endsection


