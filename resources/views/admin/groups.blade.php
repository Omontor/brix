@extends('layouts.dash')

@section('content')
<div class="main-content">
    <div class="container-fluid">  
     <div class="au-card recent-report">  
         <div class="container">
             <div class="row justify-content-center">
                <div class="col-lg-12">
                                @role('admin')

                       <h2 class="title-1">Grupos de usuarios</h2>
                       <hr class="pleca">

                       <br>

   <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                
                                                <th>Nombre  </th>
                                      
                                              
                                         
                                            </tr>
                                        </thead>
  
                                        <tbody>
                                            <tr>
                                        @forelse ($groups as $group)
                                                

                                               
                                                <td style="vertical-align: middle;"><b>{{ $group->name }}</b>
                                                    <br>
                                                <small>{{ $group->description }}</small>
                                                </td>

            
                                                
                                               </td>
                                            </tr>            
                                        @empty

                                        @endforelse                
                                        </tbody>
                                    </table>
                                </div>
                           
                             </div>
                             </div>   
                        </div>                 
                                @endrole
                            </div>
                        </div>
                    </div>

@endsection
                    