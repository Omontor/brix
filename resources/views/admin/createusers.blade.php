@extends('layouts.dash')

@section('content')

<div class="main-content">
   <div class="container-fluid">  
	 <div class="au-card recent-report">  
   		 <div class="container">
       		 <div class="row justify-content-center">
       		 	<div class="col-lg-12">
				@role('admin')
				<h2 class="title-1">Usuarios</h2>
				<hr class="pleca">
					   
				<br>					                
				<div id="app">
   <users></users>
</div>
                           
                             </div>
                             </div>   
                        </div>               			             
				@endrole
					        </div>
					    </div>
					</div>


@endsection
