@extends('layouts.dash')

@section('content')
<div class="main-content">
    <div class="container-fluid">  
     <div class="au-card recent-report">  
         <div class="container">
             <div class="row justify-content-center">
                <div class="col-lg-12">
                        @role('admin')
                       <h2 class="title-1">Productos</h2>
                       <hr class="pleca">

                       <br>
                                 <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th>Logo  </th>
                                                <th>Nombre  </th>
                                                <th>Acciones</th>
                                          
                                         
                                            </tr>
                                        </thead>
  
                                        <tbody>
                                            <tr>
                                        @forelse ($products as $product)
                                                
                                               <td>
                                                 <img src="{{ $product->logoURL }}"  class="img-fluid" style="max-height: 50px;"> 
                                                </td>
                                               
                                                <td style="vertical-align: middle;"><b>{{ $product->name }}</b>
                                           
                                        
                                                </td>
                                                <td align="left" style="vertical-align: middle;"> 
<a class="btn btn-dark btn-block" style="color: white; float: right;" href="{{route('products.show', $product->id)}}"> Ver</a>
                                                </td>
           
                                                
                                               </td>
                                            </tr>            
                                        @empty

                                        @endforelse                
                                        </tbody>
                                    </table>
                                </div>
                           
                             </div>
                             </div>   
                        </div>               
                         @endrole
                            </div>
                        </div>
                    </div>
@endsection
