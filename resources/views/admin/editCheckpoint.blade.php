@extends('layouts.dash')

@section('content')


 <div class="main-content">
    <div class="container-fluid">  
     <div class="au-card recent-report">  
         <div class="container">
             <div class="row justify-content-center">
                <div class="col-lg-12">
 @role('admin')
            <h5>Checkpoint</h5>
            <hr class="pleca">

            
            <edit-checkpoint checkpointid="{{Request::segment(2)}}"></edit-checkpoint>
        @endrole
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <div class="container">
       
    </div>
    @section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-VZUr01EdXUPZfUcj_UilKvo1DjmfGG0&libraries=places"></script> 
@endsection

@endsection



