<!DOCTYPE html>
<html lang="es">
<head>


<!-- Load jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

<!-- Load ScrollTo -->
<script src="http://flesler-plugins.googlecode.com/files/jquery.scrollTo-1.4.2-min.js"></script>

<!-- Load LocalScroll -->
<script src="http://flesler-plugins.googlecode.com/files/jquery.localscroll-1.2.7-min.js"></script>

<script type = "text/javascript">
 $(document).ready(function()
    {
        // Scroll the whole document
        $('#menuBox').localScroll({
           target:'#content'
        });
    });
</script>

    <!-- Meta Tags-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="descripción del sitio">
    <meta name="author" content="Oliver Montor">
    <meta name="keywords" content="keywords del sitio">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @auth
        <meta name="user-id" content="{{ Auth::user()->id }}">
        <meta name="user-role" content="{{ Auth::user()->roles[0]->name }}">
    @endauth

    <meta name="user-id" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'The Brix') }}</title>



    <!-- Fonts -->
    <script src="https://kit.fontawesome.com/9724818a19.js"></script>
    <!-- Styles -->



    <!-- Fontfaces CSS-->
    <link href="/css/font-face2.css" rel="stylesheet" media="all">
    <link href="/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="https://fonts.googleapis.com/css?family=Anton&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS-->
    <link href="/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="/css/theme.css" rel="stylesheet" media="all">

</head>

<style type="text/css">
    
.btn-primario {
  color: white;
  background-color: {{$clientparent[0]->primarycolor}};
}


.btn-secundario {

color: white;
  background-color: {{$clientparent[0]->secondarycolor}};

}

hr.pleca2 {

  border: 5px solid;
  border-color: {{$clientparent[0]->primarycolor}};
  width: 50%;
  margin-top: 0px;

}


}

</style>
<script src="https://code.highcharts.com/highcharts.js"></script>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="{{route('home')}}">
                            <img src="/images/logohor.png" alt="Logo" width="30%" />
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>


            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">

                          
                            <li>
                            <a href="/home">
                                <i class="fas fa-home"></i>Dashboard</a>
                            </li>

                            <br>
                    
                          <li>
                            <a href="#Inicio">
                                <i class="fas fa-home"></i>Regresar al Inicio</a>
                            </li>

                            
                            <li>
                            <a href="#InfoGral">
                                <i class="fas fa-building"></i>Información General</a>
                            </li>


   
                            <li>
                            <a href="#Estadisticos">
                                <i class="fas fa-line-chart"></i>Datos Estadísticos</a>
                            </li>

   
                            <li>
                            <a href="#Plazas">
                                <i class="fas fa-map-marker-alt"></i>Geolocalización</a>
                            </li>

                            @if($checkpoint->isstreaming != "0")
                            <li>
                            <a href="#Analytics">
                                <i class="fas fa-line-chart"></i>Analytics Web</a>
                                @else
                                @endif
                            </li>

                            @if($checkpoint->periscopeURL != "")
                            <li>
                            <a href="#Periscope">
                                <i class="fas fa-video-camera"></i>Video</a>
                                @else
                                @endif
                            </li>

                            @if($checkpoint->youtubeURL !=  "")
                            <li>
                            <a href="#Youtube">
                                <i class="fas fa-video-camera"></i>Video</a>
                            </li>
                            @else
                            @endif
   
                            <li>
                            <a href="#Galeria">
                                <i class="fas fa-file-excel"></i>Galería</a>
                            </li>
                        
           



                    </ul>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="/images/logohor.png" alt="Cool Admin" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">

                        @role('admin')
                        <li>
                            <a href="{{ url('/dashboard/view') }}">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>Usuarios</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.users')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                                                <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-building"></i>Clientes</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.clients')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                             <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-briefcase"></i>Marcas</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.products')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>



                         <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-calendar"></i>Eventos</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.checkpoints')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>

                         <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-map-marker-alt"></i>Venues</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.venues')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li> 
                            <li>
                           
                             <a href="map.html" class="btn btn-block btn-dark" style="color: white;"> + Cargar Datos</a>
                            </li>   
                        </li>
                           @endrole
                           @role('client')
                           Menú Cliente
                                                   <li>
                            <a href="{{ url('/dashboard/view') }}">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>Usuarios</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.users')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>
                           @endrole

@role('employee')

                          
                            <li>
                            <a href="/home">
                                <i class="fas fa-home"></i>Dashboard</a>
                            </li>

                            <br>
                    
                          <li>
                            <a href="#Inicio">
                                <i class="fas fa-home"></i>Regresar al Inicio</a>
                            </li>

                            
                            <li>
                            <a href="#InfoGral">
                                <i class="fas fa-building"></i>Información General</a>
                            </li>


   
                            <li>
                            <a href="#Estadisticos">
                                <i class="fas fa-line-chart"></i>Datos Estadísticos</a>
                            </li>

   
                            <li>
                            <a href="#Plazas">
                                <i class="fas fa-map-marker-alt"></i>Geolocalización</a>
                            </li>

                            @if($checkpoint->isstreaming)
                            <li>
                            <a href="#Analytics">
                                <i class="fas fa-line-chart"></i>Analytics Web</a>
                                @else
                                @endif
                            </li>

                            @if($checkpoint->periscopeURL != "")
                            <li>
                            <a href="#Periscope">
                                <i class="fas fa-video-camera"></i>Video</a>
                                @else
                                @endif
                            </li>

                            @if($checkpoint->youtubeURL)
                            <li>
                            <a href="#Youtube">
                                <i class="fas fa-video-camera"></i>Video</a>
                            </li>
                            @else
                            @endif
   
                            <li>
                            <a href="#Galeria">
                                <i class="fas fa-file-excel"></i>Galería</a>
                            </li>

                            
                            
                           @endrole

                           @role('operator')
                           Menú Operador
                                                   <li>
                            <a href="{{ url('/dashboard/view') }}">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a class="js-arrow" href="#">
                                <i class="fas fa-users"></i>Usuarios</a>
                            <ul class="list-unstyled navbar__sub-list js-sub-list">
                                                               <li>                                 
                                <a href="{{route('admin.users')}}">Ver Todos</a>
                                </li>

                                <!-- quitar para demo 
                                <li>
                                    <a href="index.html">Administradores</a>
                                </li>
                                <li>
                                    <a href="index2.html">Clientes</a>
                                </li>
                                <li>
                                    <a href="index3.html">Operadores</a>
                                </li>
                                <li>
                                    <a href="index4.html">Capturistas</a>
                                </li>
                                -->
                                <li class="btn bt-success btn-block" style="border-color: gray;">
                                    <a href="index4.html"> Crear Nuevo</a>
                                </li>
                            </ul>
                        </li>
                           @endrole

                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
           <header class="header-desktop">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">

                    
                       
                        
                        <div class="header-wrap">
                                    <!-- PARA HACER ESPACIO EN LO QUE SE HACE LA FUNCIONALIDAD DE AQUÍ-->
                               <p>  </p>
                         <p>  </p> 
                          <p>  </p> 
                           <p>  </p> 
                            <p> </p>   
                            <!--<form class="form-header" action="" method="POST">
                                <input class="au-input au-input--xl" type="text" name="search" placeholder="Buscar cliente, marca, evento o venue" />
                                <button class="au-btn--submit" type="submit">
                                    <i class="zmdi zmdi-search"></i>
                                </button>
                            </form>
                            <div class="header-button">
                                <div class="noti-wrap">
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-comment-more"></i>
                                        <span class="quantity">1</span>
                                        <div class="mess-dropdown js-dropdown">
                                            <div class="mess__title">
                                                <p>You have 2 news message</p>
                                            </div>
                                            <div class="mess__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-06.jpg" alt="Michelle Moreno" />
                                                </div>
                                                <div class="content">
                                                    <h6>Michelle Moreno</h6>
                                                    <p>Have sent a photo</p>
                                                    <span class="time">3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="mess__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-04.jpg" alt="Diane Myers" />
                                                </div>
                                                <div class="content">
                                                    <h6>Diane Myers</h6>
                                                    <p>You are now connected on message</p>
                                                    <span class="time">Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="mess__footer">
                                                <a href="#">View all messages</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-email"></i>
                                        <span class="quantity">1</span>
                                        <div class="email-dropdown js-dropdown">
                                            <div class="email__title">
                                                <p>You have 3 New Emails</p>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-06.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, 3 min ago</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-05.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, Yesterday</span>
                                                </div>
                                            </div>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="images/icon/avatar-04.jpg" alt="Cynthia Harvey" />
                                                </div>
                                                <div class="content">
                                                    <p>Meeting about new dashboard...</p>
                                                    <span>Cynthia Harvey, April 12,,2018</span>
                                                </div>
                                            </div>
                                            <div class="email__footer">
                                                <a href="#">See all emails</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="noti__item js-item-menu">
                                        <i class="zmdi zmdi-notifications"></i>
                                        <span class="quantity">3</span>
                                        <div class="notifi-dropdown js-dropdown">
                                            <div class="notifi__title">
                                                <p>You have 3 Notifications</p>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c1 img-cir img-40">
                                                    <i class="zmdi zmdi-email-open"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a email notification</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c2 img-cir img-40">
                                                    <i class="zmdi zmdi-account-box"></i>
                                                </div>
                                                <div class="content">
                                                    <p>Your account has been blocked</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__item">
                                                <div class="bg-c3 img-cir img-40">
                                                    <i class="zmdi zmdi-file-text"></i>
                                                </div>
                                                <div class="content">
                                                    <p>You got a new file</p>
                                                    <span class="date">April 12, 2018 06:50</span>
                                                </div>
                                            </div>
                                            <div class="notifi__footer">
                                                <a href="#">All notifications</a>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="account-wrap">
                                    <div class="account-item clearfix js-item-menu">
                                        <div class="image">
                                                      
                                                            <img src="{{Auth::user()->avatar}}">
        
                                        </div>
                                        <div class="content">
                                            <a class="js-acc-btn" href="#">{{ Auth::user()->name }}   </a>
                                        </div>
                                        <div class="account-dropdown js-dropdown">
                                            <div class="info clearfix">
                                                <div class="image">
                                                    <a href="#">
                                                  </a>
                                                      <img src="{{Auth::user()->avatar}}">
                                             
                                                </div>
                                                <div class="content">
                                                    <h5 class="name">
                                                        <a href="#">{{ Auth::user()->name }}   </a>
                                                    </h5>
                                                    <span class="email">{{ Auth::user()->email }}   </span>
                                                </div>
                                            </div>
                                            <!-- 
                                            <div class="account-dropdown__body">
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-account"></i>Cuenta</a>
                                                </div>
                                                <div class="account-dropdown__item">
                                                    <a href="#">
                                                        <i class="zmdi zmdi-settings"></i>Cambiar Password</a>
                                                </div>
                                            </div>
                                            -->
                                            <div class="account-dropdown__footer">
                                                <a href="{{ route('logout')}}" onclick="event.preventDefault();
                                                                             document.getElementById('logout-form').submit();">
                                                    <i class="zmdi zmdi-power"></i>Salir</a>
                                                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                            </form>

                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
                                

                       
  
            </header>

            @if (session()->has('success'))

            <div class="container">              
                 <div class="alert alert-success">{{ session('success')}}</div>
            </div>
            @endif


            @if (session()->has('warning'))

            <div class="container">              
                 <div class="alert alert-warning">{{ session('warning')}}</div>
            </div>
            @endif

                <div id="app">
    

        <main class="py-4">














<!-- Contenido del dashboard-->
<a id="Inicio"></a>   
<div class="main-content" >
  <div class="section__content section__content--p30">
    <div class="container-fluid">  

         <div class="au-card recent-report">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                    <center>
                       <img src="/images/clientlogos/{{$clientparent[0]->logoURL}}" class="img-fluid" style="max-width: 300px">
                        </center>
                     </div>
                </div>
                                  <a id="InfoGral"></a>
            </div>

      <!-- Inicia Área de Datos Duros-->   
             <div class="au-card recent-report">
                <h2 class="title-1">Información General</h2>
                <br>
                <hr class="pleca2">
                <div class="row">
       

                  <div class="col-sm-12 col-lg-6">
                    <br>
                    <center>
                    <img src="/images/productlogos/{{$productparent[0]->logoURL}}" style="max-width: 300px">
                    <br><br>
                     <h2 class="title-2">Marca:  <b>{{$clientparent[0]->name}}</h2> </b>
                     </center>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <center>
                    <img src="/images/icons/calendario.png" style="max-width: 300px;">
                     <h2 class="title-2">
                    Inicio <b> {{\Carbon\Carbon::parse($checkpoint->arrivalDate)->format('d/m/Y')}}</h2> </b> 
                    <br>
                    <h2 class="title-2">
                    Fin  <b> {{\Carbon\Carbon::parse($checkpoint->departureDate)->format('d/m/Y')}}</h2> </b> 
                    </center>
                  </div>
                    <div class="col-sm-12 col-lg-6">
                    <center>
                    <img src="/images/eventlogos/{{$checkpoint->imageURL}}" style="max-width: 500px">
                    <br><br>
                    <h2 class="title-2">Evento:  <b> {{$checkpoint->name}}</h2> </b>
                     </center>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <center>
                    <img src="/images/icons/target.png" style="max-width: 300px">
                     <h2 class="title-2">Tipo de Evento: <br> <b> Activación </h2> </b>
                     </center>
                  </div>
                </div>       
                <a id="Estadisticos"></a>
            </div>

            <!-- Inicia Área de Datos Duros-->   
     
             <div class="au-card recent-report">
                <h2 class="title-1">Datos Estadísticos</h2>
                <br>
                <hr class="pleca2">
                <div class="row">
                  <div class="col-sm-12 col-lg-12">
                         
                  </div>
                  
                    
                  <div class="col-sm-12 col-lg-4">
                             <div id="containerD" style="width:100%; height:400px;">
                                        <script>
                                        document.addEventListener('DOMContentLoaded', function () {
                                        var myChart = Highcharts.chart('containerD', {
                                            chart: {
                                                type: 'bar'
                                            },

                                            colors: ['{{$clientparent[0]->primarycolor}}', '{{$clientparent[0]->secondarycolor}}', '#000000'],
                                            title: {
                                                text: ''
                                            },
                                            xAxis: {
                                                categories: ['']
                                            },
                                            yAxis: {
                                                title: {
                                                    text: ''
                                                }
                                            },
                                            series: [{
                                                name: 'Impactos Directos',
                                                data: [{{$checkpoint->directos}}]
                                            }, {
                                                name: 'Impactos Indirectos',
                                                data: [{{$checkpoint->indirectos}}]
                                            },

                                            {
                                                name: 'Impactos Totales',
                                                data: [{{$checkpoint->indirectos + $checkpoint->directos}}]
                                            },



                                            ]
                                        });
                                    });
                                    </script>
                               
                                     </div>



                  </div>


             <div class="col-sm-12 col-lg-8">
                             <div id="containerE" style="width:100%; height:400px;">
                                        <script>
                                        document.addEventListener('DOMContentLoaded', function () {
                                        var myChart = Highcharts.chart('containerE', {
                                            chart: {
                                                  type: 'column',

                                            },

                                            colors: ['{{$clientparent[0]->primarycolor}}', '{{$clientparent[0]->secondarycolor}}'],
                                            title: {
                                                text: ''
                                            },
                                            xAxis: {
                                                categories: ['{{$checkpoint->name}}']
                                            },
                                            yAxis: {
                                                title: {
                                                    text: ''
                                                }
                                            },
                                            series: [{
                                                name: 'Días de Activación',
                                                data: [{{$checkpoint->diasactivacion}}]
                                            }, {
                                                name: 'Activaciones',
                                                data: [{{$checkpoint->totalactivaciones}}]
                                            },

                                            {
                                                name: 'Ciudades',
                                                data: [{{$checkpoint->ciudades}}]
                                            },

                                                                       {
                                                name: 'Complejos',
                                                data: [{{$checkpoint->complejos}}]
                                            },






                                            ]
                                        });
                                    });
                                    </script>
                               
                                     </div>


                            
                  </div>
                </div>
                <a id="Plazas"></a>
            </div>

           <!-- Plazas --> 
             <div class="au-card recent-report">
                <h2 class="title-1">plazas</h2>
                             <hr class="pleca2">
                <div class="row">
                  <div class="col-sm-12 col-lg-12">
                    

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var markers = [

                @forelse($venues as $venue)

    {     
        "title": '{{$venue->name}}}',
        "lat": '{{$venue->lat}}',
        "lng": '{{$venue->lng}}',
        "description": '{{$venue->name}}'
    },
    @empty
    @endforelse
    
    ];
    window.onload = function () {
        LoadMap();
    }
    function LoadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            // zoom: 8, //Not required.
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var infoWindow = new google.maps.InfoWindow();
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
 
        //Create LatLngBounds object.
        var latlngbounds = new google.maps.LatLngBounds();
 
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i]
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title
            });
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
 
            //Extend each marker's position in LatLngBounds object.
            latlngbounds.extend(marker.position);
        }
 
        //Get the boundaries of the Map.
        var bounds = new google.maps.LatLngBounds();
 
        //Center map and adjust Zoom based on the position of all markers.
        map.setCenter(latlngbounds.getCenter());
        map.fitBounds(latlngbounds);
    }
</script>
<div id="dvMap" style="width: 100%; height: 400px">
</div>


<script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-VZUr01EdXUPZfUcj_UilKvo1DjmfGG0&callback=initMap">
                        </script>
                        <br><br>
                      </div>


                  <div class="col-sm-12 col-lg-12">

                                <div class="table-responsive table--no-card m-b-40">
                                 <table class="table table-borderless table-striped table-earning">
                                  <thead>
                                    <tr>
                                    <th>Nombre</th>
                                   
                                   </tr>
                                    </thead>
                                     <tbody>
                                    <tr>
                                    @forelse ($venues as $venue)
                                        <td>{{$venue->name}}</td>

                                    </tr>            
                                    @empty
                                    @endforelse                
                                    </tbody>
                                    </table>       
                            </div>
                                                *Últimas 10 Plazas
                    <br> <br>
                        </div>
                </div>
            </div>            
<!-- Termina Área de Plazas-->  
                <a id="Analytics"></a>   
                                               
<!-- Inicia Área de Analytics--> 


@if($checkpoint->isstreaming ==  1)




        <div class="au-card recent-report"> 


               <h2 class="title-1">Estadísticas Sitio Web</h2>
                    www.navidadennogales.com<br>
                        <br>
                        <hr class="pleca2">
                            <div class="row">
                                <!-- Gráfica Barras-->
                                 <div class="col-sm-12 col-lg-6">
                                    <div id="container" style="width:100%; height:400px;">
                                        <script>
                                        document.addEventListener('DOMContentLoaded', function () {
                                        var myChart = Highcharts.chart('container', {

                                          colors: ['{{$clientparent[0]->primarycolor}}', '{{$clientparent[0]->secondarycolor}}'],
                                            chart: {
                                                type: 'bar'
                                            },
                                            title: {
                                                text: ''
                                            },
                                            xAxis: {
                                                categories: ['Fuentes']
                                            },
                                            yAxis: {
                                                title: {
                                                    text: ''
                                                }
                                            },
                                            series: [{
                                                name: '{{$topreferrers[0]['url']}}',
                                                data: [{{$topreferrers[0]['pageViews']}}]
                                            }, {
                                                name: 'URL Directa',
                                                data: [{{$topreferrers[1]['pageViews']}}]
                                            }]
                                        });
                                    });
                                    </script>
                                </div>
                            </div>
                           <!-- Gráfica Columnas-->

                            <div class="col-sm-12 col-lg-6">
                                 <div id="container2" style="width:100%; height:400px;">
                                    <script>
                                    document.addEventListener('DOMContentLoaded', function () {
                                        var myChart = Highcharts.chart('container2', {
                                          colors: ['{{$clientparent[0]->primarycolor}}', '{{$clientparent[0]->secondarycolor}}, {{$clientparent[0]->primarycolor}}', '{{$clientparent[0]->secondarycolor}}'],
                                            chart: {
                                                type: 'column'
                                            },
                                            title: {
                                                text: ''
                                            },
                                            xAxis: {
                                                categories: ['Navegadores']
                                            },
                                            yAxis: {
                                                title: {
                                                    text: ''
                                                }
                                            },
                                            series: [
                                           @forelse ($topbrowsers as $topbrowser)
                                            {
                                                
                                                name: '{{$topbrowser['browser']}}',
                                                data: [{{$topbrowser['sessions']}}]

                                            }, 
                                             @empty
                                                @endforelse
                                                ]
                                        });
                                    });
                                    </script>
                                </div>
                            </div>
                            <!-- Gráfica Línea-->
                            <div class="col-sm-12 col-lg-6">
                                 <div id="container3" style="width:100%; height:400px;">
                                    <script>
                                 Highcharts.chart('container3', {
                                                                    colors: ['{{$clientparent[0]->secondarycolor}}, {{$clientparent[0]->primarycolor}}', '{{$clientparent[0]->secondarycolor}}'],
                                        chart: {
                                            type: 'bar'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        subtitle: {
                                            text: 'Visitas',
                                            align: 'right',
                                            verticalAlign: 'bottom'
                                        },
                                        legend: {
                                            layout: 'vertical',
                                            align: 'left',
                                            verticalAlign: 'top',
                                            x: 100,
                                            y: 70,
                                            floating: true,
                                            borderWidth: 1,
                                            backgroundColor:
                                                Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
                                        },
                                        xAxis: {
                                            categories: [
                                            @forelse ($totalVisitors as $totalvisitor)
                                            {{\Carbon\Carbon::parse($totalvisitor['date'])->format('d')}}
                                            ,
                                            @empty
                                            @endforelse
                                            ]
                                        },
                                        yAxis: {
                                            title: {
                                                text: 'Visitas'
                                            }
                                        },
                                        plotOptions: {
                                            area: {
                                                fillOpacity: 0.5
                                            }
                                        },
                                        credits: {
                                            enabled: false
                                        },
                                        series: [{

                                            name: 'Visitantes',
                                            data: [ @forelse ($totalVisitors as $totalvisitor) {{$totalvisitor['visitors']}}
                                            ,
                                            @empty
                                            @endforelse

                                            ]
                                        },
                                        {
                                            name: 'Páginas Vistas',
                                            data: [ @forelse ($totalVisitors as $totalvisitor) {{$totalvisitor['pageViews']}}
                                            ,
                                            @empty
                                            @endforelse

                                            ]
                                        },
                                        ]
                                    }); 
                                </script>
                                </div>
                            </div>
                            <!-- Gráfica Circular-->
                            <div class="col-sm-12 col-lg-6">
                                <br>
                                 <div id="container4" style="width:100%; height:400px;">
                                    <script>
                                        Highcharts.chart('container4', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                                                  colors: ['{{$clientparent[0]->primarycolor}}', '{{$clientparent[0]->secondarycolor}}, {{$clientparent[0]->primarycolor}}', '{{$clientparent[0]->secondarycolor}}'],
                                                title: {
                                                    text: 'Usuarios'
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                accessibility: {
                                                    point: {
                                                        valueSuffix: 'visitas'
                                                    }
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                                                        }
                                                    }
                                                },
                                                series: [
                                                
                                                {
                                                    name: '',
                                                    colorByPoint: true,
                                                    data: [

                                                    @forelse ($usertypes as $usertype){
                                                 
                                                        name: '{{$usertype['type']}}',
                                                        y: {{$usertype['sessions']}},
                                                        sliced: false,
                                                        selected: false

                                                    },

                                                    @empty
                                                    @endforelse
                                                    ]
                                                      
                                                }
                                                
                                                ]

                                            });
                                    </script>
                                    
                                </div>
                                    <center>
                                        
                                        @forelse($usertypes as $usertype)
                                        <h3>
                                        {{$usertype['type']}} <b>  {{$usertype['sessions']}} </b>
                                        <br>
                                        </h3>
                                        @empty
                                        @endforelse
                                    </center>                          
                            </div>    
                        </div>
                        <!-- Tabla de visitas 1 -->   
                         <h2 class="title-1">Visitas a navidadennogales.com</h2>
                             <hr class="pleca2">
                                <div class="table-responsive table--no-card m-b-40">
                                 <table class="table table-borderless table-striped table-earning">
                                  <thead>
                                    <tr>
                                    <th>Fecha</th>
                                    <th>Visitors</th>
                                     <th>Page Views</th>
                                    </tr>
                                    </thead>
                                     <tbody>
                                    <tr>
                                    @forelse ($totalVisitors as $totalvisitor)
                                        <td>{{$totalvisitor['date']->diffForHumans(null, false, false, 3)}}</td>
                                        <td>{{$totalvisitor['visitors']}}</td>
                                        <td>{{$totalvisitor['pageViews']}}</td>
                                    </tr>            
                                    @empty
                                    @endforelse                
                                    </tbody>
                                    </table>       
                            </div>
                        <!-- Tabla de visitas 2 -->    


                             @else
                             @endif

  
                    </div>
              <a id="Periscope"></a>   
                 

                <!-- Termina Área de Analytics-->    
            @if($checkpoint->periscopeURL)
                <!-- Inicia Streaming-->  
                        <div class="au-card recent-report">
                            <div class="col-sm-12 col-lg-12" style="height: auto;">
                                <h2 class="title-1">Video</h2>
                                 <hr class="pleca2">
                                 <br>
                                    <div>
                                        <video width="100%" height="600" controls>
  <source src="/images/routesvideos/{{$checkpoint->id}}/{{$checkpoint->periscopeURL}}.mp4" type="video/mp4">
  <source src="/images/routesvideos/{{$checkpoint->id}}/{{$checkpoint->periscopeURL}}}.ogg" type="video/ogg">
</video>
                            </div>
                            
                        </div>

                @else
                @endif
                <a id=Youtube"></a>   

                <!-- Inicia Video-->  

                @if($checkpoint->youtubeURL)
                
                            <div class="au-card recent-report">
                                <div class="col-sm-12 col-lg-12">
                                         <h2 class="title-1">Video</h2>
                                <hr class="pleca2">

                                <!-- Inicia el código de youtube-->
                                 <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
                        
                                    <div id="player" class="embed-responsive embed-responsive-16by9">
                                    <div class="embed-responsive-item">
                                    <script>
                                      // 2. This code loads the IFrame Player API code asynchronously.
                                      var tag = document.createElement('script');

                                      tag.src = "https://www.youtube.com/iframe_api";
                                      var firstScriptTag = document.getElementsByTagName('script')[0];
                                      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                                      // 3. This function creates an <iframe> (and YouTube player)
                                      //    after the API code downloads.
                                      var player;
                                      function onYouTubeIframeAPIReady() {
                                        player = new YT.Player('player', {
                                          
                                          width: '100%',
                                          height: '100%',
                                          videoId: '{{$checkpoint->youtubeURL}}',
                                         playerVars: { 'autoplay': 1, 'controls': 0,  'loop':1},
                                          events: {
                                            'onReady': onPlayerReady,
                                            'onStateChange': onPlayerStateChange
                                          }
                                        });
                                      }

                                      // 4. The API will call this function when the video player is ready.
                                      function onPlayerReady(event) {
                                        event.target.stopVideo();
                                        player.mute();
                                      }

                                      // 5. The API calls this function when the player's state changes.
                                      //    The function indicates that when playing a video (state=1),
                                      //    the player should play for six seconds and then stop.
                                      var done = false;
                                      function onPlayerStateChange(event) {
                                        if (event.data == YT.PlayerState.PLAYING && !done) {
                                          setTimeout(stopVideo, 6000);
                                          done = true;
                                        }
                                      }
                                      function stopVideo() {
                                        player.playVideo();
                                      }
                                    </script>
                                    </div>
                                  </div>
                                </div> 
                            </div>
                            @else
                            @endif
                            <a id="Galeria"></a>  
             <!-- Inicia Galería-->  
                                    
                         <div class="au-card recent-report">
                                     <div class="col-sm-12 col-lg-12">

                                         <h2 class="title-1">Galería</h2>
                                <hr class="pleca2">

<div class="container">
  <div class="row">
    <div class="col-sm">
      <img src="/images/routesimages/{{$checkpoint->id}}/1.jpeg" class="img-fluid " style="margin: 5px; object-fit: cover; max-width: auto; max-height:500px;">
    </div>
    <div class="col-sm">
         <img src="/images/routesimages/{{$checkpoint->id}}/2.jpeg" class="img-fluid" style="margin: 5px; object-fit: cover; max-width: auto; max-height:500px;">
    </div>
    <div class="col-sm">
         <img src="/images/routesimages/{{$checkpoint->id}}/3.jpeg" class="img-fluid" style="margin: 5px; object-fit: cover; max-width: auto; max-height:500px;">
    </div>
  </div>
    <div class="row">
    <div class="col-sm">
      <img src="/images/routesimages/{{$checkpoint->id}}/4.jpeg" class="img-fluid" style="margin: 5px; object-fit: cover; max-width: auto: max-height:500px;">
    </div>
    <div class="col-sm">
         <img src="/images/routesimages/{{$checkpoint->id}}/5.jpeg" class="img-fluid" style="margin: 5px; object-fit: cover; max-width: auto; max-height:500px;">
    </div>
    <div class="col-sm">
         <img src="/images/routesimages/{{$checkpoint->id}}/6.jpeg" class="img-fluid" style="margin: 5px; object-fit: cover; max-width: auto; max-height:500px;">
    </div>
  </div>
    <div class="row">
    <div class="col-sm">
      <img src="/images/routesimages/{{$checkpoint->id}}/7.jpeg" class="img-fluid" style="margin: 5px; object-fit: cover; max-width: auto; max-height:500px;">
    </div>
    <div class="col-sm">
         <img src="/images/routesimages/{{$checkpoint->id}}/8.jpeg" class="img-fluid" style="margin: 5px; object-fit: cover; max-width: auto; max-height:500px;">
    </div>
    <div class="col-sm">
         <img src="/images/routesimages/{{$checkpoint->id}}/9.jpeg" class="img-fluid" style="margin: 5px; object-fit: cover; max-width: auto; max-height:500px;">
    </div>
  </div>
</div>



                                </div>
                            </div> 
                                  

                    
            </div>
        </div>
    </div>
</div>


  
       </main>

  
                               
                                <div class="copyright">
                                    <p>    {{config('app.name')}} | Copyright  {{date('Y')}}</small></p>
                                </div>
                    

</div>
  <!-- Jquery JS-->
    <script src="/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="/vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="/vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="/vendor/slick/slick.min.js">
    </script>
    <script src="/vendor/wow/wow.min.js"></script>
    <script src="/vendor/animsition/animsition.min.js"></script>
    <script src="/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="/vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="/vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="/js/main.js"></script>

@yield('scripts')


</body>


</html>
