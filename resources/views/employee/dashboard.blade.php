@extends('layouts.dash')

@section('content')
    <div class="container">
        @role('employee')
            <div id="period" data-period="{{$period}}"></div>
            <div id="checkinsdays" data-checkinsdays="{{$checkinsdays}}"></div>
            <div class="row">
                <div class="col-md-12">
                    <div id="chartcheckinscalendar"></div>
                </div>
            </div>

            <div class="row" style="margin-top: 30px;">
                <div class="col-md-6">
                    <div id="chartcheckins" style="min-width: 310px; height: 400px; max-width: 800px"></div>
                </div>
                <div class="col-md-6">
                    <div class="table-wrapper-scroll-y my-custom-scrollbar">
                        <table class="table table-bordered table-hover">
                            <tbody>
                                @foreach ($checkins as $checkin)
                                    <tr>
                                        <td>
                                            <kbd>{{$checkin->userName}}</kbd>
                                            ha hecho check-in en
                                            <span class="badge badge-info">{{$checkin->checkpointName}}</span>
                                            a las <strong>{{\Carbon\Carbon::parse($checkin->created_at)->timezone('America/Mexico_City')}}</strong>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                    <h2>Imagenes recientes</h2>
                    @if (count($images) > 0)
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="background-color: #8080806e;">
                            <div class="carousel-inner">
                                @foreach ($images as $index => $image)
                                    <div class="carousel-item {{($index ===0) ? 'active': ''}}">
                                        <img class="d-block w-100" src="{{$image->imageUrl}}" alt="..." style="object-fit: contain; width: 600px; height:400px">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h5>{{$index}} {{$image->userName}}</h5>
                                            <p>Checkin en {{$image->checkpointName}}</p>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    @else
                        <div class="alert alert-info">
                            No hay imagenes por el momento
                        </div>
                    @endif

                </div>
            </div>
        @endrole
    </div>
@endsection
@section('scripts')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>

    <script>
        $(function() {
            let period = JSON.parse(document.querySelector('#period').dataset.period);
            let checkinsdays = JSON.parse(document.querySelector('#checkinsdays').dataset.checkinsdays);


            Highcharts.chart('chartcheckins', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Checkpoints'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}'
                        },
                        showInLegend: true
                    }
                },
                series: [{
                name: 'Checkpoints',
                colorByPoint: true,
                data: [{
                    name: 'Hechos',
                    y: {{$checkinsDone}},
                    sliced: true,
                    selected: true
                }, {
                    name: 'Retardos',
                    y: {{$checkinsDelay}}
                }, {
                    name: 'Faltas',
                    y: {{$checkinsLack}}
                }]
                }]
            });
            Highcharts.chart('chartcheckinscalendar', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Checkins por día'
                },
                xAxis: {
                    categories: period,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Número de Checkins'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y} checkins</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Hechos',
                    data: checkinsdays.done

                }, {
                    name: 'Retardos',
                    data: checkinsdays.delay

                }, {
                    name: 'Faltas',
                    data: checkinsdays.lack

                }]
            });
        });


    </script>
@endsection
