@extends('layouts.dash')

@section('content')
    <div class="container">
        <div class="row">
            @role('operator')
                <h4>Checkpoints</h4>
                <div class="col-md-12">
                    <checkpoints-table uri="/op/checkpoints"></checkpoints-table>
                </div>
            @endrole
        </div>
    </div>
@endsection
