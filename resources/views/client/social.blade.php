<script src="https://code.highcharts.com/highcharts.js"></script>
@extends('layouts.dash')
@section('content')
<!-- Contenido del dashboard-->
<div class="main-content" >
	<div class="section__content section__content--p30">
		<div class="container-fluid">  
			<!-- Inicia Área de Datos Duros-->   
             <div class="au-card recent-report">
                <h2 class="title-1">Información General</h2>
                <br>
                <hr class="pleca">
                <div class="row">
                  <div class="col-sm-12 col-lg-6">
                    <img src="/images/brix.png">
                     <h2 class="title-2">Marca:  <b>Nombre de la marca </h2> </b>

                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <img src="/images/brix.png">
                  </div>
                    <div class="col-sm-12 col-lg-6">
                    <img src="/images/brix.png">
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <img src="/images/brix.png">
                  </div>
                </div>
            </div>
            <!-- Inicia Área de Datos Duros-->   
             <div class="au-card recent-report">
                <h2 class="title-1">Datos Estadísticos</h2>
                <br>
                <hr class="pleca">
                <div class="row">
                  <div class="col-sm-12 col-lg-6">
                    <img src="/images/brix.png">
                     <h2 class="title-2">Marca:  <b>Nombre de la marca </h2> </b>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <img src="/images/brix.png">
                  </div>
                    <div class="col-sm-12 col-lg-6">
                    <img src="/images/brix.png">
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <img src="/images/brix.png">
                  </div>
                </div>
            </div>

           <!-- Plazas --> 
             <div class="au-card recent-report">
                <h2 class="title-1">plazas</h2>
                             <hr class="pleca">
                <div class="row">
                  <div class="col-sm-12 col-lg-12">
                    <style>
                      /* Always set the map height explicitly to define the size of the div
                       * element that contains the map. */
                          #map {
                            height: 500;
                          }
                          /* Optional: Makes the sample page fill the window. */
                        </style>
                        <div id="map"></div>
                        <script>
                          var map;
                          function initMap() {
                            map = new google.maps.Map(
                                document.getElementById('map'),
                                {center: new google.maps.LatLng(19.384620, -99.166340), zoom: 16});

                            var iconBase =
                                'https://developers.google.com/maps/documentation/javascript/examples/full/images/';

                            var icons = {
                              pin: {
                                icon: '/images/brixpin.png',
                                
                              },
                            };


                            var latlngList = [];
                            var features = [
                            

                                {
                                position: new google.maps.LatLng(19.384620, -99.166340),
                                type: 'pin'
                            }
                            ];
                            // Create markers.
                            for (var i = 0; i < features.length; i++) {
                              var marker = new google.maps.Marker({
                                position: features[i].position,
                                icon: icons[features[i].type].icon,
                                map: map
                              });
                            };
                            map.fitBounds(bounds);
                            map.setCenter(bounds.getCenter());
                          }
                    </script>
                    <script async defer
                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-VZUr01EdXUPZfUcj_UilKvo1DjmfGG0&callback=initMap">
                    </script>
                    <br>
                  </div>
                  <div class="col-sm-12 col-lg-12">
                                <div class="table-responsive table--no-card m-b-40">
                                 <table class="table table-borderless table-striped table-earning">
                                  <thead>
                                    <tr>
                                    <th>Fecha</th>
                                   </tr>
                                    </thead>
                                     <tbody>
                                    <tr>
                                    @forelse ($totalVisitors as $totalvisitor)
                                        <td>{{$totalvisitor['date']->diffForHumans(null, false, false, 3)}}</td>
                                    </tr>            
                                    @empty
                                    @endforelse                
                                    </tbody>
                                    </table>       
                            </div>
                        </div>
                </div>
            </div>            
<!-- Termina Área de Plazas-->  
                                               
<!-- Inicia Área de Analytics-->    
        <div class="au-card recent-report"> 
               <h2 class="title-1">Estadísticas Sitio Web</h2>
                    Rango: Últimos 15 Días<br>
                        <br>
                        <hr class="pleca">
                            <div class="row">
                                <!-- Gráfica Barras-->
                                 <div class="col-sm-12 col-lg-6">
                                    <div id="container" style="width:100%; height:400px;">
                                        <script>
                                        document.addEventListener('DOMContentLoaded', function () {
                                        var myChart = Highcharts.chart('container', {
                                            chart: {
                                                type: 'bar'
                                            },
                                            title: {
                                                text: ''
                                            },
                                            xAxis: {
                                                categories: ['Fuentes']
                                            },
                                            yAxis: {
                                                title: {
                                                    text: ''
                                                }
                                            },
                                            series: [{
                                                name: '{{$topreferrers[0]['url']}}',
                                                data: [{{$topreferrers[0]['pageViews']}}]
                                            }, {
                                                name: 'URL Directa',
                                                data: [{{$topreferrers[1]['pageViews']}}]
                                            }]
                                        });
                                    });
                                    </script>
                                </div>
                            </div>
                           <!-- Gráfica Columnas-->

                            <div class="col-sm-12 col-lg-6">
                                 <div id="container2" style="width:100%; height:400px;">
                                    <script>
                                    document.addEventListener('DOMContentLoaded', function () {
                                        var myChart = Highcharts.chart('container2', {
                                            chart: {
                                                type: 'column'
                                            },
                                            title: {
                                                text: ''
                                            },
                                            xAxis: {
                                                categories: ['Navegadores']
                                            },
                                            yAxis: {
                                                title: {
                                                    text: ''
                                                }
                                            },
                                            series: [
                                           @forelse ($topbrowsers as $topbrowser)
                                            {
                                                
                                                name: '{{$topbrowser['browser']}}',
                                                data: [{{$topbrowser['sessions']}}]

                                            }, 
                                             @empty
                                                @endforelse
                                                ]
                                        });
                                    });
                                    </script>
                                </div>
                            </div>
                            <!-- Gráfica Línea-->
                            <div class="col-sm-12 col-lg-6">
                                 <div id="container3" style="width:100%; height:400px;">
                                    <script>
                                 Highcharts.chart('container3', {
                                        chart: {
                                            type: 'area'
                                        },
                                        title: {
                                            text: ''
                                        },
                                        subtitle: {
                                            text: 'Visitas',
                                            align: 'right',
                                            verticalAlign: 'bottom'
                                        },
                                        legend: {
                                            layout: 'vertical',
                                            align: 'left',
                                            verticalAlign: 'top',
                                            x: 100,
                                            y: 70,
                                            floating: true,
                                            borderWidth: 1,
                                            backgroundColor:
                                                Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
                                        },
                                        xAxis: {
                                            categories: [
                                            @forelse ($totalVisitors as $totalvisitor)
                                            {{\Carbon\Carbon::parse($totalvisitor['date'])->format('d')}}
                                            ,
                                            @empty
                                            @endforelse
                                            ]
                                        },
                                        yAxis: {
                                            title: {
                                                text: 'Visitas'
                                            }
                                        },
                                        plotOptions: {
                                            area: {
                                                fillOpacity: 0.5
                                            }
                                        },
                                        credits: {
                                            enabled: false
                                        },
                                        series: [{

                                            name: 'Visitantes',
                                            data: [ @forelse ($totalVisitors as $totalvisitor) {{$totalvisitor['visitors']}}
                                            ,
                                            @empty
                                            @endforelse

                                            ]
                                        },
                                        {
                                            name: 'Páginas Vistas',
                                            data: [ @forelse ($totalVisitors as $totalvisitor) {{$totalvisitor['pageViews']}}
                                            ,
                                            @empty
                                            @endforelse

                                            ]
                                        },
                                        ]
                                    }); 
                                </script>
                                </div>
                            </div>
                            <!-- Gráfica Circular-->
                            <div class="col-sm-12 col-lg-6">
                                <br>
                                 <div id="container4" style="width:100%; height:400px;">
                                    <script>
                                        Highcharts.chart('container4', {
                                                chart: {
                                                    plotBackgroundColor: null,
                                                    plotBorderWidth: null,
                                                    plotShadow: false,
                                                    type: 'pie'
                                                },
                                                title: {
                                                    text: 'Usuarios'
                                                },
                                                tooltip: {
                                                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                },
                                                accessibility: {
                                                    point: {
                                                        valueSuffix: 'visitas'
                                                    }
                                                },
                                                plotOptions: {
                                                    pie: {
                                                        allowPointSelect: true,
                                                        cursor: 'pointer',
                                                        dataLabels: {
                                                            enabled: true,
                                                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                                                        }
                                                    }
                                                },
                                                series: [
                                                
                                                {
                                                    name: '',
                                                    colorByPoint: true,
                                                    data: [

                                                    @forelse ($usertypes as $usertype){
                                                 
                                                        name: '{{$usertype['type']}}',
                                                        y: {{$usertype['sessions']}},
                                                        sliced: false,
                                                        selected: false

                                                    },

                                                    @empty
                                                    @endforelse
                                                    ]
                                                      
                                                }
                                                
                                                ]

                                            });
                                    </script>
                                    
                                </div>
                                    <center>
                                        
                                        @forelse($usertypes as $usertype)
                                        <h3>
                                        {{$usertype['type']}} <b>  {{$usertype['sessions']}} </b>
                                        <br>
                                        </h3>
                                        @empty
                                        @endforelse
                                    </center>                          
                            </div>    
                        </div>
                        <!-- Tabla de visitas 1 -->   
                         <h2 class="title-1">Visitas por día</h2>
                             <hr class="pleca">
                                <div class="table-responsive table--no-card m-b-40">
                                 <table class="table table-borderless table-striped table-earning">
                                  <thead>
                                    <tr>
                                    <th>Fecha</th>
                                    <th>Visitors</th>
                                     <th>Page Views</th>
                                    </tr>
                                    </thead>
                                     <tbody>
                                    <tr>
                                    @forelse ($totalVisitors as $totalvisitor)
                                        <td>{{$totalvisitor['date']->diffForHumans(null, false, false, 3)}}</td>
                                        <td>{{$totalvisitor['visitors']}}</td>
                                        <td>{{$totalvisitor['pageViews']}}</td>
                                    </tr>            
                                    @empty
                                    @endforelse                
                                    </tbody>
                                    </table>       
                            </div>
                        <!-- Tabla de visitas 2 -->    
                                <h2 class="title-1">Visitas a páginas</h2>
                                <hr class="pleca">
                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th>Fecha</th>
                                                <th>Título de Página</th>
                                                <th>Visitors</th>
                                                <th>Page Views</th>

                                            </tr>
                                        </thead>


                                        <tbody>
                                            <tr>
                                        @forelse ($visitors as $visitor)

                                                <td>{{$visitor['date']}}</td>
                                                <td>{{$visitor['pageTitle']}}</td>
                                                <td>{{$visitor['visitors']}}</td>
                                                <td>{{$visitor['pageViews']}}</td>
                                            
                                            </tr>            
                                        @empty

                                        @endforelse                
                                        </tbody>
                                    </table>
                             </div>
                    </div>
                <!-- Termina Área de Analytics-->    

                <!-- Inicia Streaming-->  
                        <div class="au-card recent-report">
                            <div class="col-sm-12 col-lg-12" style="height: auto;">
                                <h2 class="title-1">Streaming</h2>
                                 <hr class="pleca">
                                 <br>
                                    <div>
                                        <center> 
                                    <img src="/images/perislogo.png" class="img-fluid" width="150px">
                                    <br>
                                    <br><br>
                                    <h2 class="title-2">Ningún streaming disponible al momento</h2> 
                                    </div>   
                                    <br>
                                    </center>
                            </div>
                        </div>
                <!-- Inicia Video-->  
                            <div class="au-card recent-report">
                                <div class="col-sm-12 col-lg-12">
                                         <h2 class="title-1">Video</h2>
                                <hr class="pleca">

                                <!-- Inicia el código de youtube-->
                                 <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
                        
                                    <div id="player" class="embed-responsive embed-responsive-16by9">
                                    <div class="embed-responsive-item">
                                    <script>
                                      // 2. This code loads the IFrame Player API code asynchronously.
                                      var tag = document.createElement('script');

                                      tag.src = "https://www.youtube.com/iframe_api";
                                      var firstScriptTag = document.getElementsByTagName('script')[0];
                                      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                                      // 3. This function creates an <iframe> (and YouTube player)
                                      //    after the API code downloads.
                                      var player;
                                      function onYouTubeIframeAPIReady() {
                                        player = new YT.Player('player', {
                                          
                                          width: '100%',
                                          height: '100%',
                                          videoId: 'qLJyqWoDVv0',
                                         playerVars: { 'autoplay': 1, 'controls': 0,  'loop':1},
                                          events: {
                                            'onReady': onPlayerReady,
                                            'onStateChange': onPlayerStateChange
                                          }
                                        });
                                      }

                                      // 4. The API will call this function when the video player is ready.
                                      function onPlayerReady(event) {
                                        event.target.stopVideo();
                                        player.mute();
                                      }

                                      // 5. The API calls this function when the player's state changes.
                                      //    The function indicates that when playing a video (state=1),
                                      //    the player should play for six seconds and then stop.
                                      var done = false;
                                      function onPlayerStateChange(event) {
                                        if (event.data == YT.PlayerState.PLAYING && !done) {
                                          setTimeout(stopVideo, 6000);
                                          done = true;
                                        }
                                      }
                                      function stopVideo() {
                                        player.playVideo();
                                      }
                                    </script>
                                    </div>
                                  </div>
                                </div>
                            </div>
                            

             <!-- Inicia Galería-->  
                                    
                         <div class="au-card recent-report">
                                     <div class="col-sm-12 col-lg-12">
                                         <h2 class="title-1">Galería</h2>
                                <hr class="pleca">
                                </div>
                            </div>                                   		   
                                      
            				
            </div>
        </div>
    </div>
</div>


	







@endsection