@include('includes/header_account')

        <!-- Begin page -->
        
        <div class="wrapper-page">

            <div class="card" >
                <div class="card-body" >

                    <h3 class="text-center m-0">
                        <a href="index" class="logo logo-admin"><img src="images/brix_logo.png" height="250" alt="logo"></a>
                    </h3>

                    <div class="p-3">
                          <center> <p>Introduce tus credeciales para inciar sesión</p>
                            <br></center>
                         

                        <form method="POST" action="{{ route('login') }}">
                        @csrf                           


                        <div class="form-group row">

                            <label for="email" class="col-md-4 col-form-label text-md-right">e-mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        {{--  <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>  --}}

                        
                         
                                <br>
                                <button type="submit" class="btn btn-warning btn-block">
                                    Iniciar Sesión
                                    <br>
                                </button>

                                {{--  @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif  --}}
                          
                       
                    </form>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <small>
        {{config('app.name')}} |  {{date('Y')}} | Todos los Derechos Reservados </small>
            </div>

        </div>

@include('includes/footer_account')