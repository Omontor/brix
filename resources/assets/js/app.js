
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import VueGoodTablePlugin from 'vue-good-table';
import 'vue-good-table/dist/vue-good-table.css'
import Notifications from 'vue-notification'

window.Vue = require('vue');
Vue.use(VueGoodTablePlugin);
Vue.use(Notifications);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('users', require('./components/users/Users.vue').default);
Vue.component('users-table', require('./components/users/UsersTable.vue').default);
Vue.component('new-user', require('./components/users/NewUser.vue').default);

Vue.component('products-table', require('./components/products/ProductsTable.vue').default);
Vue.component('new-client-product', require('./components/products/NewProductByClient.vue').default);

Vue.component('new-form', require('./components/forms/NewForm.vue').default);
Vue.component('new-question', require('./components/forms/NewQuestion.vue').default);
Vue.component('show-form', require('./components/forms/Form.vue').default);
Vue.component('answerable-form', require('./components/forms/AnswerableForm.vue').default);
Vue.component('show-question', require('./components/forms/ShowQuestion.vue').default);
Vue.component('show-options', require('./components/forms/ShowOptions.vue').default);

Vue.component('new-route', require('./components/routes/NewRoute.vue').default);
Vue.component('routes', require('./components/routes/Routes.vue').default);
Vue.component('clients-table', require('./components/clients/ClientsTable.vue').default);
Vue.component('new-client', require('./components/clients/NewClient.vue').default)

Vue.component('checkpoints-table', require('./components/checkpoints/CheckpointsTable.vue').default);
Vue.component('new-checkpoint', require('./components/checkpoints/NewCheckpoint.vue').default);
Vue.component('edit-checkpoint', require('./components/checkpoints/EditCheckpoint').default);
Vue.component('gg-map', require('./components/checkpoints/ggmap.vue').default);
Vue.component('user-groups', require('./components/users/Groups.vue').default);
Vue.component('new-group', require('./components/users/NewGroup.vue').default);
Vue.component('edit-group', require('./components/users/EditGroup.vue').default);


Vue.prototype.$userId = document.querySelector("meta[name='user-id']").getAttribute('content');
Vue.prototype.$userRole = document.querySelector("meta[name='user-role']").getAttribute('content');

const app = new Vue({
    el: '#app',
    methods: {
        showNotification(title, text,  type='success'){
            this.$notify({
                group: 'foo',
                title: title,
                type: type,
                speed: 450,
                text: text
            });
        }
    }
});


window.Vue = require('vue');



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});
