import Errors from './Errors';

class Form{
    constructor(data){
        this.originalData = data;

        for(let field in data){
            this[field] = data[field];
        }

        this.errors = new Errors();
    }
    data(){
        let data = {}; 
        
        for (let property in this.originalData){
            data[property] = this[property];
        }

         return data; 
    }
    reset(){
        for (let field in this.originalData) {
            this[field] = '';
        }
        this.errors.clear();
    }
    submit(requestType, url, reset = true){
        return new Promise((resolve, reject) => {
            axios[requestType](url, this.data())
                .then(response => {
                    if (reset) {
                        this.onSuccess(response.data);
                    }
                    resolve(response.data);
                })
                .catch(error => {
                    this.onFail(error);
                    reject(error);
                });
        })
        
    }
    onSuccess(data){
        this.reset();
    }
    onFail(error){
        this.errors.record(error.response.data);
    }
}

export default Form;