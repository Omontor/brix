import NewOption from '../forms/NewOption';

class QuestionOption{
    constructor(optionType, ref){
        this.optionType = optionType;
        this.ref = ref;
        this.instance = {};
        this.setComponent();
    }

    setComponent(){
        let props = {
            optionType: this.optionType,
            name: '',
        };
        let OptionComponent = Vue.extend(NewOption);
        
        let instance = new OptionComponent({ 
            propsData: props
        }).$mount();

        this.instance = instance;

        this.ref.appendChild(instance.$el);
    }
}

export default QuestionOption;