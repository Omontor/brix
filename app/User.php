<?php

namespace App;

use App\Models\AnsweredForm;
use App\Models\Checkin;
use App\Models\Client;
use App\Models\Product;
use App\Models\ProductUser;
use App\Models\UserGroup;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'contact','isActive','user_groups_id','client_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'isActive' => 'boolean'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_user', 'user_id', 'product_id');
    }

    public function group()
    {
        return $this->belongsTo(UserGroup::class, 'user_groups_id');
    }

    public function checkins()
    {
        return $this->hasMany(Checkin::class);
    }

    public function answeredForms()
    {
        return $this->hasMany(AnsweredForm::class);
    }
 /**
     * Redes Sociales
     */

    public function profiles() {

        
        return $this->hasMany(SocialProfile::class);


    }


    public function getAvatarAttribute() {


        return optional($this->profiles->last())->avatar ?? url('/images/icons/avatar.png');

    }
}
