<?php

namespace App\Http\Controllers\Operator;

use Analytics;
use Spatie\Analytics\Period;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Checkpoint;
use App\Models\Checkin;
use App\Models\Client;
use App\Models\Product;
use App\Venue;

class CheckpointsController extends Controller
{



    public function show(Checkpoint $checkpoint){


        $clientparent = Client::where('id',$checkpoint->client_id)->get();
        $productparent = Product::where('id',$checkpoint->client_id)->get();
        $visitors = Analytics::fetchVisitorsAndPageViews(Period::days(15));
        $totalVisitors = Analytics::fetchTotalVisitorsAndPageViews(Period::days(5));
        $mostvisited = Analytics::fetchMostVisitedPages(Period::days(5));
        $topreferrers = Analytics::fetchTopReferrers(Period::days(15));
        $usertypes = Analytics::fetchUserTypes(Period::days(15));
        $topbrowsers = Analytics::fetchTopBrowsers(Period::days(15));
        $venues = Venue::where('checkpoint_id',$checkpoint->id)->get();



        //Revisar este método'
        $analyticsData = Analytics::performQuery(
           Period::months(1),
              'ga:sessions',
              [
                  'metrics' => 'ga:sessions, ga:pageviews',
                  'dimensions' => 'ga:yearMonth',
              ]
        );



             return view('checkpointdashboard', [

                'checkpoint' =>  $checkpoint,

                   //variables de google analytics
            'visitors' => $visitors,
             'mostvisited' => $mostvisited,
             'totalVisitors' => $totalVisitors,
             'topreferrers' => $topreferrers,
             'usertypes' => $usertypes,
             'topbrowsers' => $topbrowsers,
             'analyticsData' => $analyticsData,
            'clientparent' => $clientparent,
            'productparent' => $productparent,
            'venues' => $venues,

             ]);
             

        }

    public function index(Request $request)
    {
        $user = Auth::user();

        $userGroup = $user->group;

        $checkpoints = Checkpoint::with([
            'route',
            'form' => function($query){
                $query->with(['questions' => function($query){
                    $query->with(['options' ]);
                }]);
            },
            'checkins' => function($query) use($user){
                $query->where('user_id', $user->id)
;            },
            'answeredForms' => function($query) use($user){
                $query->where('user_id', $user->id)
;            },
        ])
        ->where('user_groups_id', $userGroup->id);


        if($request->query('name') && $request->query('filter') !== 'undefined'){
            $checkpoints = $checkpoints->searchName($request->query('name'));
        }
        if($request->query('fullAddress') && $request->query('filter') !== 'undefined'){
            $checkpoints = $checkpoints->searchFullAddress($request->query('fullAddress'));
        }
        if($request->query('routeName') && $request->query('filter') !== 'undefined'){
            $checkpoints = $checkpoints->searchRouteName($request->query('routeName'));
        }

        $checkpoints = $checkpoints->orderBy('created_at', 'desc')->paginate(15);

        return response()->json($checkpoints, 200,[], JSON_NUMERIC_CHECK);
    }

    public function checkin($checkpointId)
    {
        try{
            $operator = Auth::user();
            $checkpoint = Checkpoint::find($checkpointId);

            $status = Checkin::calcStatus($checkpoint->arrivalDate, $checkpoint->departureDate);

            Checkin::create([
                'user_id' => $operator->id,
                'checkpoint_id' => $checkpoint->id,
                'status' => $status
            ]);

            $checkpoint = Checkpoint::with([
                'route',
                'form' => function($query){
                    $query->with(['questions' => function($query){
                        $query->with(['options']);
                    }]);
                },
                'checkins' => function($query) use($operator){
                    $query->where('user_id', $operator->id);
                },
                'answeredForms' => function($query) use($operator){
                    $query->where('user_id', $operator->id);
                },
            ])->where('id', $checkpoint->id)->first();

            return response()->json($checkpoint, 201);
        } catch (\Exception $exception){
            if ($exception->getMessage() == 'EARLY_CHECKIN'){
                return response()->json([
                    'message' => [
                        'error' => 'EARLY_CHECKIN',
                        'friendly' => 'No puedes hacer checkin antes de la hora de llegada.'
                    ]
                ], 400);
            } else {
                return response()->json([
                    'code' => 'SERVER',
                    'error' => $exception->getMessage(),
                    'line' => $exception->getLine()
                ], 500);
            }
        }
    }
}
