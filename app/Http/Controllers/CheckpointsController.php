<?php

namespace App\Http\Controllers;


use App\Models\Checkpoint;
use App\Models\Form;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CheckpointsController extends Controller
{
    public function index(Request $request)
    {
        $checkpoints = Checkpoint::with([
            'route', 
            'form' => function($query){
                $query->with(['questions' => function($query){
                    $query->with(['options' ]);
                }]);
            }
        ]);

        if($request->query('name') && $request->query('filter') !== 'undefined'){
            $checkpoints = $checkpoints->searchName($request->query('name'));
        }
        if($request->query('fullAddress') && $request->query('filter') !== 'undefined'){
            $checkpoints = $checkpoints->searchFullAddress($request->query('fullAddress'));
        }
        if($request->query('routeName') && $request->query('filter') !== 'undefined'){
            $checkpoints = $checkpoints->searchRouteName($request->query('routeName'));
        }

        $checkpoints = $checkpoints->orderBy('created_at', 'desc')->paginate(15);

        return response()->json($checkpoints, 200,[], JSON_NUMERIC_CHECK);

    }


    

    public function show($checkpointId)
    {
        $checkpoint = Checkpoint::with([
            'route' => function($query){
                $query->with(['product']);
            }, 
            'form' => function($query){
                $query->with(['questions' => function($query){
                    $query->with(['options']);
                }]);
            }
        ])->where('id', $checkpointId)->first();

        return response()->json($checkpoint, 200,[], JSON_NUMERIC_CHECK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'fullAddress' => 'required',
            'arrivalDate' => 'required|date',
            'departureDate' => 'required|date|after:arrivalDate',
            'routeId' => 'required',
            'formId' => 'required',
            'groupId' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $formLayout = Form::find($request->formId);
        $newForm = $formLayout->cloneToForm();

        $checkpoint = Checkpoint::create([
            'name' => $request->name,
            'fullAddress' => $request->fullAddress,
            'arrivalDate' => $request->arrivalDate,
            'departureDate' => $request->departureDate,
            'ggMapUri' => $request->ggMapUri,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'route_id' => $request->routeId,
            'form_id' => $newForm->id,
            'user_groups_id' => $request->groupId
        ]);

        return response()->json($checkpoint, 201);
    }

    public function update(Request $request, $checkpointId)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'fullAddress' => 'required',
            'arrivalDate' => 'required|date',
            'departureDate' => 'required|date|after:arrivalDate',
            'routeId' => 'required',
            'groupId' => 'required'
        ]);
        
        $checkpoint = Checkpoint::with([
            'route' => function($query){
                $query->with(['product']);
            }, 
            'form' => function($query){
                $query->with(['questions' => function($query){
                    $query->with(['options' ]);
                }]);
            }
        ])->where('id', $checkpointId)->first();

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        if ($checkpoint->form_id !== $request->form_id) {
            $formLayout = Form::find($request->formId);
            $newForm = $formLayout->cloneToForm();
            $checkpoint->form_id = $newForm->id;
        }
        
        $checkpoint->name = $request->name;
        $checkpoint->fullAddress = $request->fullAddress;
        $checkpoint->arrivalDate = $request->arrivalDate;
        $checkpoint->departureDate = $request->departureDate;
        $checkpoint->ggMapUri = $request->ggMapUri;
        $checkpoint->lat = $request->lat;
        $checkpoint->lng = $request->lng;
        $checkpoint->route_id = $request->routeId;
        $checkpoint->user_groups_id = $request->groupId;
        
        $checkpoint->save();

        return response()->json($checkpoint, 200);
    }


    public function dashboard(Checkpoint $checkpoint){

        

             return view('checkpointdashboard', [

                'checkpoints' =>  Checkpoint::all(),

             ]);


        }
}
