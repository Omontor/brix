<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FormsController extends Controller
{

    public function index(Request $request)
    {
        $forms = Form::with('questions');

        if($request->query('mode')){
            $forms = $forms
                ->where('mode', $request->query('mode'));
        }

        $forms = $forms->orderBy('created_at', 'desc')->get();

        return response()->json($forms, 200, [], JSON_NUMERIC_CHECK);
    }

    public function show($id)
    {
        $form = Form::with([
            'questions' => function($query){
                $query->with(['options']);
        }])->where('id', $id)->first();

        return response()->json($form, 200);
    }

    public function store(Request $request)
    {
       try {
           $formTypes = implode(',', config('formTypes'));
           $validator = Validator::make($request->all(),[
               'mode' => 'required|in:' . $formTypes,
               'title' => 'required|unique:users,email',
               'description' => 'required',
               'questions' => 'required|array'
           ]);

           if ($validator->fails()) {
               return response()->json($validator->errors(), 400);
           }

           $form = Form::create([
               'mode' => $request->mode,
               'title' => $request->title,
               'description' => $request->description
           ]);

           $questions = collect($request->questions);

           $questions->each(function ($q, $key) use($form){
                $question = Question::create([
                    'name' => $q['name'],
                    'questionType' => $q['questionType'],
                    'order' => $key + 1,
                    'form_id' => $form->id
                ]);
                if ($question->questionType == 'SELECT' || $question->questionType == 'RADIO' || $question->questionType == 'CHECKBOX') {
                    $options = collect($q['options']);
                    $options->each(function($o, $key) use ($question){
                        if (!empty($o['name'])) {
                            $question->options()->create([
                                'name' => $o['name']
                            ]);
                        }
                    });
                }
            });

            $form = Form::with([
                'questions' => function($query){
                        $query->with('options');
                    }
                ])
                ->where('id', $form->id)
                ->first();

           return response()->json($form, 201, ['location' => '/forms' . $form->id]);

       } catch (\Exception $exception){
           return response()->json([
               'code' => 'SERVER',
               'error' => $exception->getMessage(),
               'line' => $exception->getLine()
           ]);
       }
    }
}
