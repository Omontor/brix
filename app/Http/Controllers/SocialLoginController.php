<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Socialite;
use App\SocialProfile;
use Illuminate\Http\Request;

class SocialLoginController extends Controller
{
    public function redirectToSocialNetwork($socialNetwork)
    {
        return Socialite::driver($socialNetwork)->redirect();
    }

    public function handleSocialNetworkCallback($socialNetwork)
    {
        try
        {
            $socialUser = Socialite::driver($socialNetwork)->user();
        }
        catch (\Exception $e)
        {
            return redirect()->route('login')->with('warning', 'Hubo un error en el login...');
        }

        $socialProfile = SocialProfile::firstOrNew([
            'social_network' => $socialNetwork,
            'social_network_user_id' => $socialUser->getId()
        ]);

        if ( ! $socialProfile->exists )
        {
            $user = User::firstOrNew(['email' => $socialUser->getEmail()]);

            if (! $user->exists )
            {
                $user->name = $socialUser->getName();
                $user->assignRole('employee');
                $user->save();
            }

            $socialProfile->avatar = $socialUser->getAvatar();

            $user->profiles()->save( $socialProfile );
        }

        Auth::login($socialProfile->user);
        return redirect()->route('home')->with('success', 'Bienvenido ' . $socialProfile->user->name);
    }
}
