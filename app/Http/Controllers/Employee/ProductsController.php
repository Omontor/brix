<?php

namespace App\Http\Controllers\Employee;

use App\Models\Route;
use App\Models\Checkpoint;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{


    public function show(Product $product){

             return view('admin.productdashboard', [
                'product' => $product,
                'checkpoints' =>  Checkpoint::latest('created_at', 'DESC')->paginate(5),
                'routes' =>  Route::latest('created_at', 'DESC')->paginate(5)

             ]);
        }

    public function index(Request $request)
    {
        $user = Auth::user();

        $employeProducts = $user->products;

        $products = Product::where('id', $employeProducts->toArray())->with(['client']);

        if($request->query('name') && $request->query('filter') !== 'undefined'){
            $products = $products->searchName($request->query('name'));
        }
        if($request->query('code') && $request->query('filter') !== 'undefined'){
            $products = $products->searchCode($request->query('code'));
        }

        $products = $products->paginate(15);

        return response()->json($products, 200,[], JSON_NUMERIC_CHECK);
    }

    public function destroy($id)
    {
        try{
            $product = Product::find($id);
            $product->isActive = !$product->isActive;
            $product->save();

            return response()->json('No Content', 204);
        } catch (\Exception $exception){
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }
    public function store(Request $request, $userId)
    {
        try{
            if ($userId){
                $user = User::find($userId);
            }
            else{
                $user = Auth::user();
            }

            if (!$user->hasRole('client')){
                return response()->json([
                    'code' => 'IS_NOT_CLIENT',
                    'message' => 'This user can not have products'
                ],400);
            }
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'code' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $product = $user->products()->create($request->all());

            return response()->json([
                'data' => $product
            ],201, [
                'location' => '/users/' .$user->id . '/products/' . $product->id
            ]);

        } catch(\Exception $exception){
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }
}
