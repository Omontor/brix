<?php

namespace App\Http\Controllers\Employee;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function dashboard($productId)
    {
        $product = Product::find($productId);

        $checkinsDone   = Product::countCheckins($productId, 'DONE');
        $checkinsLack   = Product::countCheckins($productId, 'LACK');
        $checkinsDelay  = Product::countCheckins($productId, 'DELAY');

        $checkins = Product::productCheckins($productId);

        $images = Product::checkpointImages($productId);
        $endDate = Carbon::now()->addDay()->format('Y-m-d');
        $startDate = Carbon::now()->subDays(15)->format('Y-m-d');

        $checkinsdaysDone = Product::checkpointsPerDay($productId, 'DONE', $startDate, $endDate);
        $checkinsdaysDelay = Product::checkpointsPerDay($productId, 'DELAY', $startDate, $endDate);
        $checkinsdaysLack = Product::checkpointsPerDay($productId, 'LACK', $startDate, $endDate);


        $period = CarbonPeriod::create($startDate, $endDate)->toArray();
        $period = collect($period);
        $period = $period->map(function ($day){
            return $day->format('Y-m-d');
        });

        $statsCheckinsDone = $this->fillCollection($checkinsdaysDone, $period);
        $statsCheckinsDelay = $this->fillCollection($checkinsdaysDelay, $period);
        $statsCheckinsLack = $this->fillCollection($checkinsdaysLack, $period);

        $checkinsdays = collect([
            'done' => $statsCheckinsDone,
            'delay' => $statsCheckinsDelay,
            'lack' => $statsCheckinsLack
        ]);
        return view('employee.dashboard', [
            'checkinsDone' => $checkinsDone,
            'checkinsDelay' => $checkinsDelay,
            'checkinsLack' => $checkinsLack,
            'checkins' => $checkins,
            'images' => $images,
            'checkinsdays' => $checkinsdays,
            'period' => $period
        ]);
    }

    private function fillCollection($checkins, $period)
    {
        $results = collect();
        $period->each(function($day) use($checkins,$results){
            if ($checkins->has($day)){
                $results->push($checkins[$day]);
            } else{
                $results->push(0);
            }
        });
        return $results;

    }
}
