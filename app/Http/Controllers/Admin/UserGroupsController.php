<?php

namespace App\Http\Controllers\Admin;

use App\Models\UserGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserGroupsController extends Controller
{
    public function index(Request $request)
    {
        $groups = UserGroup::orderBy('created_at', 'desc');

        if($request->query('name') && $request->query('filter') !== 'undefined'){
            $groups = $groups->where('name', 'like', '%' . $request->query('name') . '%');;
        }

        if ($request->query('page')) {
            $groups = $groups->paginate(15);
        } else{
            $groups = $groups->get();
        }

        return response()->json($groups, 200);

    }

    public function index2() {

        $groups = UserGroup::orderBy('created_at', 'desc');

             return view('/groups/view', [
            'groups' => $groups,

        ]);
    }



    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required|unique:user_groups,name',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $userGroup = UserGroup::create($request->all());

        return response()->json($userGroup,201);
    }

    public function destroy($groupId)
    {
        $group = UserGroup::find($groupId);

        if ($group->users()->exists()) {
            return response()->json([
                'message' => [
                    'error' => 'RELATIONSHIP_EXISTS',
                    'friendly' => 'No se puede borrar porque tiene relación con otros registros.',
                ]
            ], 400);
        } else if ($group->checkpoints()->exists()) {
            return response()->json([
                'message' => [
                    'error' => 'RELATIONSHIP_EXISTS',
                    'friendly' => 'No se puede borrar porque tiene relación con otros registros.',
                ]
            ], 400);
        }

        $group->delete();

        return response()->json('No Content', 204);
    }

    public function update(Request $request, $groupId)
    {
        $group = UserGroup::find($groupId);

        $group->update($request->all());

        return response()->json($group, 200);
    }
}
