<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::with(['client','routes']);
        if($request->query('name') && $request->query('filter') !== 'undefined'){
            $products = $products->searchName($request->query('name'));
        }
        if($request->query('code') && $request->query('filter') !== 'undefined'){
            $products = $products->searchCode($request->query('code'));
        }
        if($request->query('client') && $request->query('filter') !== 'undefined'){
            $products = $products->searchClientName($request->query('client'));
        }
        if ($request->query('isActive')) {
            $products = $products->where('isActive', boolval($request->query('isActive')));
        }

        $products = $products->orderBy('created_at', 'desc');

        if ($request->query('page')){
            $products = $products->paginate(15);
        } else {
            $products = $products->get();
        }

        return response()->json($products, 200,[], JSON_NUMERIC_CHECK);
    }

    public function destroy($id)
    {
        try{
            $product = Product::find($id);
            $product->isActive = !$product->isActive;
            $product->save();

            return response()->json('No Content', 204);
        } catch (\Exception $exception){
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }

    public function update(Request $request, $productId)
    {
        try {
            $product = Product::find($productId);

            $product->name = $request->name;
            $product->code = $request->code;
            $product->isActive = $request->isActive;

            $product->save();

            return response()->json([
                'data' => $product
            ], 200);
            
        } catch (\Exception $exception) {
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }

    }

         public function show(Product $product){

             return view('productdashboard', [
                'product' => $product,

             ]);
        }
}
