<?php

namespace App\Http\Controllers\Admin;

use App\Models\AnsweredForm;
use App\Models\Checkpoint;
use App\Models\HelperFile;
use App\Models\Option;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class FormController extends Controller
{
    public function store(Request $request)
    {
        $operator = Auth::user();
        $checkpoint = Checkpoint::find($request->checkpointId);

        $answeredForm = $operator->answeredForms()->create([
            'form_id' => $request->formId,
            'checkpoint_id' => $checkpoint->id
        ]);
        $options = $request->options;

        foreach ($options as $key => $opt){
            $params = explode('_', $key);
            $questionId = $params[0];
            $questionType = $params[1];

            if ($questionType === 'TEXT'){
                $answeredForm->answers()->create([
                    'text' => $opt,
                    'question_id' => $questionId
                ]);
            } else if ($questionType === 'RADIO' || $questionType === 'CHECKBOX'){
                $optionId = $params[2];
                $option = Option::find($optionId);
                $answeredForm->answers()->create([
                    'option_id' => $option->id,
                    'question_id' => $questionId
                ]);
            }
            else if ($questionType === 'SELECT'){
                $option = Option::find($opt);
                $answeredForm->answers()->create([
                    'option_id' => $option->id,
                    'question_id' => $questionId
                ]);
            }
            else if ($questionType === 'IMAGE'){
                $imageFile = $request->file('options');
                $imageFile = $imageFile[$key];
                $pathImage = HelperFile::storeFile($imageFile, 'form-images');
                $answeredForm->answers()->create([
                    'text' => $pathImage,
                    'question_id' => $questionId
                ]);
            }
        }
        $checkpoint = $checkpoint = Checkpoint::with([
            'route',
            'form' => function($query){
                $query->with(['questions' => function($query){
                    $query->with(['options' ]);
                }]);
            },
            'checkins' => function($query) use($operator){
                $query->where('user_id', $operator->id)
;            },
            'answeredForms' => function($query) use($operator){
                $query->where('user_id', $operator->id)
;            },
        ])->where('id', $request->checkpointId)->first();

        return response()->json([
            'answeredForm' => $answeredForm,
            'checkpoint' => $checkpoint
        ], 201);

    }
}
