<?php

namespace App\Http\Controllers\Admin;

use Analytics;
use Spatie\Analytics\Period;
use App\Models\Answer;
use App\Models\Checkin;
use App\Models\Product;
use App\Models\Route;
use App\Models\Client;
use App\Models\Checkpoint;
use App\Venue;
use App\User;
use App\Models\UserGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class DashboardController extends Controller
{
    public function dashboard()
    {
        $operators  = User::role('operator')->count();
        $employees  = User::role('employee')->count();
        $clients     = Client::count();
        
        $checkinsDone   = Checkin::where('status', 'DONE')->count();
        $checkinsDelay  = Checkin::where('status', 'DELAY')->count();
        $checkinsLack   = Checkin::where('status', 'LACK')->count();

        $products = Product::count();

        $routes = Route::count();

        $answers = Answer::whereHas('question', function ($query){
            $query->where('questionType','IMAGE');
        })->count();

        $usertotals = User::latest('created_at', 'DESC')->paginate(10);        
        $grouptotals = UserGroup::latest('id', 'desc')->paginate(5);
        $checkpoints = Checkpoint::latest('id', 'DESC')->paginate(10);  
        $clientstotal    = Client::latest('id', 'DESC')->paginate(10);  
        $productstotal = Product::latest('id', 'DESC')->paginate(10); 
        $clientescompletos = Client::all();
        $productoscompletos = Product::all();
        $venues = Venue::latest('id', 'DESC')->paginate(10);


        //Métodos de Google Analytics


        $visitors = Analytics::fetchVisitorsAndPageViews(Period::days(1));
        $totalVisitors = Analytics::fetchTotalVisitorsAndPageViews(Period::days(1));
        $mostvisited = Analytics::fetchMostVisitedPages(Period::days(1));
        $topreferrers = Analytics::fetchTopReferrers(Period::days(1));
        $usertypes = Analytics::fetchUserTypes(Period::days(1));
        $topbrowsers = Analytics::fetchTopBrowsers(Period::days(1));

        

        return view('admin.dashboard', [
            'operators' => $operators,
            'employees' =>$employees,
            'clients' => $clients,
            'checkinsDone' => $checkinsDone,
            'checkinsDelay' => $checkinsDelay,
            'checkinsLack' => $checkinsLack,
            'products' => $products,
            'routes' => $routes,
            'answers' => $answers,
            'usertotals' => $usertotals,
            'grouptotals' => $grouptotals,
            'checkpoints' => $checkpoints,
            'clientstotal' => $clientstotal,
            'productstotal' => $productstotal,
            'clientescompletos'=> $clientescompletos,
            'productoscompletos' => $productoscompletos,
            'venues' => $venues,   

            //variables de google analytics
            'visitors' => $visitors,
             'mostvisited' => $mostvisited,
             'totalVisitors' => $totalVisitors,
             'topreferrers' => $topreferrers,
             'usertypes' => $usertypes,


        ]);


    }

    public function dashboard2 (){

        $clients    = Client::latest('id', 'DESC')->paginate(10);  


        return view('admin.clients', [
            'clients' => $clients,

    ]);

    }

        public function productos (){

        $clients    = Client::latest('id', 'DESC')->paginate(10);  
        $products    = Product::latest('id', 'DESC')->paginate(10);  


        return view('admin.products', [
            'products' => $products,
            'clients' => $clients,

    ]);

    }


        public function usuarios (){
        
        $users = User::latest('created_at', 'DESC')->paginate(10);   

        return view('admin.users', [
            'users' => $users,

    ]);

    }

            public function grupos (){
        
        $groups = UserGroup::latest('id', 'desc')->paginate(5);

        return view('admin.groups', [
            'groups' => $groups,

    ]);

    }

}
