<?php

namespace App\Http\Controllers\Admin;


use App\Models\Product;
use App\Models\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RoutesController extends Controller
{
    public function index(Request $request)
    {
        $routes = Route::where('product_id', $request->query('productId'))->get();

        return response()->json($routes, 200,[], JSON_NUMERIC_CHECK);
    }
    public function store(Request $request, $clientId, $productId)
    {
        try{
            $product = Product::find($productId);

            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $route = $product->routes()->create($request->all());

            return response()->json([
                'data' => $route
            ],201, [
                'location' => "/clients/{$clientId}/products/{$product->id}/routes/{$route->id}"
            ]);

        } catch(\Exception $exception){
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }

    public function destroy($routeId)
    {
        $route = Route::find($routeId);
        
        if ($route->checkpoints()->exists()) {
            return response()->json([
                'message' => [
                    'error' => 'RELATIONSHIP_EXISTS',
                    'friendly' => 'No se puede borrar porque tiene relación con otros registros.',
                ]
            ], 400);
        }

        $route->delete();

        return response()->json('No Content', 204);
    }

    public function update(Request $request, $routeId)
    {
        $route = Route::find($routeId);

        $route->name = $request->name;

        $route->save();

        return response()->json([
            'data' => $route
        ], 200);
    }
}
