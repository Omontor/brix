<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\UserGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{

    public function index(Request $request)
    {
        $users = User::with(['roles','client','group', 'products']);

        if($request->query('name') && $request->query('filter') !== 'undefined'){
            $users = $users
                ->where('name', 'like', '%' . $request->query('name') . '%');
        }
        if($request->query('email') && $request->query('filter') !== 'undefined'){
            $users = $users
                ->where('email', 'like', '%' . $request->query('email') . '%');
        }
        $users = $users->orderBy('created_at', 'desc')->paginate(15);
        return response()->json($users, 200,[], JSON_NUMERIC_CHECK);
    }



    

    public function store(Request $request)
    {
        try{

            $roles = implode(',', config('roles'));
            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'password' => 'required',
                'role' => 'required|in:' . $roles,
                'client_id' => 'required_if:role,employee',
                'product_id' => 'required_if:role,employee',
                'group_id' => 'required_if:role,operator'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'contact' => $request->contact,
                'password' => Hash::make($request->password)
            ]);

            $user->assignRole($request->role);
            if ($request->role === 'employee'){
                $user->products()->attach($request->product_id);
                $user->client_id = $request->client_id;
            }
            if ($request->role === 'operator'){
                $user->user_groups_id = $request->group_id;
            }
            $user->save();
            $user = User::with(['roles', 'client', 'group', 'products'])->where('id', $user->id)->first();

            return response()->json(
                $user,
                201, [
                'location' => '/users' . $user->id
            ]);

        } catch (\Exception $exception){
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }

    public function destroy($id)
    {
        try{
            $user = User::find($id);
            $user->isActive = !$user->isActive;
            $user->save();

            return response()->json('No Content', 204);
        } catch (\Exception $exception){
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }

    public function update(Request $request, $userId)
    {
        try {
            $user = User::find($userId);
            $user->name = $request->name;
            $user->contact = $request->contact;
            if ($request->password) {
                $user->password = Hash::make($request->password);
            }
            if ($user->hasRole('operator') && $request->group['id']){
                $user->user_groups_id = $request->group['id'];
            }
            if ($user->hasRole('employee') && $request->client_id){
                $user->products()->detach();
                $user->products()->attach($request->products[0]['id']);
                $user->client_id = $request->client_id;
            }

            $user->save();
            $user = User::with(['roles', 'group','client', 'products'])->where('id', $user->id)->first();

            return response()->json($user, 200);


        } catch (\Exception $exception){
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ],500);
        }
    }
}
