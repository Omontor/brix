<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ClientProductsController extends Controller
{
    public function index($clientId)
    {
        $client = Client::find($clientId);
        $products = $client->products;

        return response()->json([
            'data' => $products
        ]);
    }
    public function store(Request $request, $clientId)
    {
        try{
            $client = Client::find($clientId);

            $validator = Validator::make($request->all(),[
                'name' => 'required',
                'code' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $product = $client->products()->create($request->all());

            return response()->json([
                'data' => $product
            ],201, [
                'location' => '/clients/' .$client->id . '/products/' . $product->id
            ]);

        } catch(\Exception $exception){
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }
}
