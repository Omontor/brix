<?php

namespace App\Http\Controllers\Client;


use Analytics;
use Spatie\Analytics\Period;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialController extends Controller
{
    public function social(){



        $visitors = Analytics::fetchVisitorsAndPageViews(Period::days(15));
        $totalVisitors = Analytics::fetchTotalVisitorsAndPageViews(Period::days(15));
        $mostvisited = Analytics::fetchMostVisitedPages(Period::days(15));
        $topreferrers = Analytics::fetchTopReferrers(Period::days(15));
        $usertypes = Analytics::fetchUserTypes(Period::days(15));
        $topbrowsers = Analytics::fetchTopBrowsers(Period::days(15));

        //Revisar este método'
        $analyticsData = Analytics::performQuery(
   Period::months(1),
      'ga:sessions',
      [
          'metrics' => 'ga:sessions, ga:pageviews',
          'dimensions' => 'ga:yearMonth',
      ]
);


        return view('client.social', [

            //variables de google analytics
            'visitors' => $visitors,
             'mostvisited' => $mostvisited,
             'totalVisitors' => $totalVisitors,
             'topreferrers' => $topreferrers,
             'usertypes' => $usertypes,
             'topbrowsers' => $topbrowsers,
             'analyticsData' => $analyticsData,
        ]);

    }
}
