<?php

namespace App\Http\Controllers;

use App\Models\Checkpoint;
use App\Models\Product;
use App\Models\Client;
use App\Models\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientsController extends Controller
{
    public function index(Request $request)
    {

        $clients = Client::orderBy('name', 'asc');

        if($request->query('name') && $request->query('filter') !== 'undefined'){
            $clients = $clients
                ->where('name', 'like', '%' . $request->query('name') . '%');
        }

        if ($request->query('page')){
            $clients = $clients->paginate(15);
        } else {
            $clients = $clients->get();
        }

        return response()->json($clients, 200);
    }

    public function destroy($id)
    {
        try{
            $client = Client::find($id);
            $client->isActive = !$client->isActive;
            $client->save();

            return response()->json('No Content', 204);
        } catch (\Exception $exception){
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }

    public function store(Request $request)
    {
        try{

            $validator = Validator::make($request->all(),[
                'name' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), 400);
            }

            $client = Client::create([
                'name' => $request->name,
                'description' => $request->description,
                'isActive' => true
            ]);

            return response()->json([
                'data' => $client
            ],201, [
                'location' => "/clients/{$client->id}"
            ]);

        } catch(\Exception $exception){
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }

    public function update(Request $request, $clientId)
    {
        try {
            $client = Client::find($clientId);

            $client->name = $request->name;
            $client->description = $request->description;
            $client->isActive = $request->isActive;

            $client->save();

            return response()->json([
                'data' => $client
            ],200);
        } catch (\Exception $exception) {
            return response()->json([
                'code' => 'SERVER',
                'error' => $exception->getMessage(),
                'line' => $exception->getLine()
            ]);
        }
    }

        public function show(Client $client){

             return view('clientsdashboard', [
                'client' => $client,
                'products' => Product::where('client_id', $client->id)->get(),
                'routes' =>  Route::where('client_id', $client->id)->get(),
                'checkpoints' =>  Checkpoint::where('client_id', $client->id)->get(),

                'productscount' =>  Product::count(),
                'routescount' => Route::count(),
                'checkpointscount' => Route::count(),


             ]);


        }


          
}
