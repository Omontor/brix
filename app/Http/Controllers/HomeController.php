<?php

namespace App\Http\Controllers;


use Analytics;
use Spatie\Analytics\Period;
use App\Models\Answer;
use App\Models\Checkin;
use App\Models\Product;
use App\Models\Route;
use App\Models\Client;
use App\Models\Checkpoint;
use App\Venue;
use App\User;
use App\Models\UserGroup;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       $operators  = User::role('operator')->count();
        $employees  = User::role('employee')->count();
        $clients     = Client::all();
        
        $checkinsDone   = Checkin::where('status', 'DONE')->count();
        $checkinsDelay  = Checkin::where('status', 'DELAY')->count();
        $checkinsLack   = Checkin::where('status', 'LACK')->count();

        $products = Product::first('id', 'DESC')->paginate(2); 

        $routes = Route::all();
        $answers = Answer::whereHas('question', function ($query){
            $query->where('questionType','IMAGE');
        })->count();

        $usertotals = User::latest('created_at', 'DESC')->paginate(10);        
        $grouptotals = UserGroup::latest('id', 'desc')->paginate(5);
        $checkpoints = Checkpoint::first('id', 'asc')->paginate(3);  
        $clientstotal    = Client::latest('id', 'DESC')->paginate(10);  
        $productstotal = Product::latest('id', 'DESC')->paginate(10); 
        $clientescompletos = Client::latest('id', 'asc')->paginate(2); 
        $productoscompletos = Product::first('id', 'asc')->paginate(2); 
        $venues = Venue::latest('id', 'DESC')->paginate(10);


        //Métodos de Google Analytics


        $visitors = Analytics::fetchVisitorsAndPageViews(Period::days(1));
        $totalVisitors = Analytics::fetchTotalVisitorsAndPageViews(Period::days(1));
        $mostvisited = Analytics::fetchMostVisitedPages(Period::days(1));
        $topreferrers = Analytics::fetchTopReferrers(Period::days(1));
        $usertypes = Analytics::fetchUserTypes(Period::days(1));
        $topbrowsers = Analytics::fetchTopBrowsers(Period::days(1));

        

        return view('home', [
            'operators' => $operators,
            'employees' =>$employees,
            'clients' => $clients,
            'checkinsDone' => $checkinsDone,
            'checkinsDelay' => $checkinsDelay,
            'checkinsLack' => $checkinsLack,
            'products' => $products,
            'routes' => $routes,
            'answers' => $answers,
            'usertotals' => $usertotals,
            'grouptotals' => $grouptotals,
            'checkpoints' => $checkpoints,
            'clientstotal' => $clientstotal,
            'productstotal' => $productstotal,
            'clientescompletos'=> $clientescompletos,
            'productoscompletos' => $productoscompletos,
            'venues' => $venues,   

            //variables de google analytics
            'visitors' => $visitors,
             'mostvisited' => $mostvisited,
             'totalVisitors' => $totalVisitors,
             'topreferrers' => $topreferrers,
             'usertypes' => $usertypes,


        ]);



    }
}
