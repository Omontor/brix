<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $fillable = ['name', 'description'];

    public function users()
    {
        return $this->hasMany(User::class,'user_groups_id');
    }

    public function checkpoints()
    {
        return $this->hasMany(Checkpoint::class, 'user_groups_id');
    }
}
