<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    protected $fillable = ['name', 'code', 'isActive', 'client_id'];
    protected $casts = [
        'isActive' => 'boolean'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function routes()
    {
        return $this->hasMany(Route::class);
    }

    public function employees()
    {
        return $this->belongsToMany(User::class, 'product_user', 'user_id', 'product_id');
    }

    /**
     * @param $query
     * @param $name
     * @return mixed
     */
    public function scopeSearchName($query, $name)
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    /**
     * @param $query
     * @param $code
     * @return mixed
     */
    public function scopeSearchCode($query, $code)
    {
        return $query->where('code', 'like', '%' . $code . '%');
    }

    /**
     * @param $query
     * @param $customer String be name or email
     * @return mixed
     */
    public function scopeSearchClientName($query, $client)
    {
        return $query->whereHas('client', function($query) use($client){
            $query
                ->where('name', 'like', '%' . $client. '%');
        });
    }

    public static function countCheckins($productId, $status)
    {
        return DB::table('products')
            ->join('routes', 'routes.product_id', '=', 'products.id')
            ->join('checkpoints', 'checkpoints.route_id', '=', 'routes.id')
            ->join('checkins', 'checkins.checkpoint_id', '=', 'checkpoints.id')
            ->where('products.id', $productId)
            ->where('checkins.status', $status)
            ->count();
    }

    public static function productCheckins($productId)
    {
        return DB::table('products')
            ->join('routes', 'routes.product_id', '=', 'products.id')
            ->join('checkpoints', 'checkpoints.route_id', '=', 'routes.id')
            ->join('checkins', 'checkins.checkpoint_id', '=', 'checkpoints.id')
            ->join('users', 'users.id', '=', 'checkins.user_id')
            ->where('products.id', $productId)
            ->orderBy('checkins.created_at', 'desc')
            ->select('checkins.*', 'users.id as userId', 'users.name as userName','checkpoints.name as checkpointName', 'checkpoints.fullAddress')
            ->take(15)->get();
    }

    public static function checkpointImages($productId)
    {
        return DB::table('products')
            ->join('routes', 'routes.product_id', '=', 'products.id')
            ->join('checkpoints', 'checkpoints.route_id', '=', 'routes.id')
            ->join('checkins', 'checkins.checkpoint_id', '=', 'checkpoints.id')
            ->join('users', 'users.id', '=', 'checkins.user_id')
            ->join('answered_forms', 'users.id', '=', 'answered_forms.user_id')
            ->join('answers', 'answers.answered_form_id', '=', 'answered_forms.id')
            ->join('questions', 'questions.id', '=', 'answers.question_id')
            ->where('products.id', $productId)
            ->where('questions.questionType', 'IMAGE')
            ->orderBy('checkins.created_at', 'desc')
            ->select('checkins.*', 'users.id as userId', 'users.name as userName', 'checkpoints.name as checkpointName' ,'checkpoints.fullAddress', 'answers.text as imageUrl')
            ->take(15)->get();
    }

    public static function checkpointsPerDay($productId, $status,$startDate, $endDate)
    {
        $results =  DB::table('products')
            ->join('routes', 'routes.product_id', '=', 'products.id')
            ->join('checkpoints', 'checkpoints.route_id', '=', 'routes.id')
            ->join('checkins', 'checkins.checkpoint_id', '=', 'checkpoints.id')
            ->where('products.id', $productId)
            ->where('status', $status)
            ->whereBetween('checkins.created_at', [$startDate, $endDate])
            ->select(
                DB::raw('DATE(checkins.created_at) as date'),
                DB::raw('count(*) as checkedins'))
            ->groupBy('date')
            ->get();
        return $results->mapWithKeys(function($res){
            return [$res->date => $res->checkedins];
        });
    }

}
