<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = ['text', 'answered_form_id', 'question_id', 'option_id'];

    public function form()
    {
        return $this->belongsTo(AnsweredForm::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
