<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['name', 'questionType', 'order' ,'form_id'];

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function options()
    {
        return $this->hasMany(Option::class);
    }

    public function isSelectable()
    {
        return $this->questionType == 'SELECT' || $this->questionType == 'RADIO' || $this->questionType == 'CHECKBOX';
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
