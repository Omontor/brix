<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AnsweredForm extends Model
{
    protected $fillable = ['form_id', 'checkpoint_id'];
    public function operator()
    {
        return $this->belongsTo(User::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function checkpoint()
    {
        return $this->belongsTo(Checkpoint::class);
    }
}
