<?php

namespace App\Models;


use Illuminate\Support\Str;

class HelperFile
{
    public static function storeFile($file, $path)
    {
        if(!is_dir(public_path($path))){
            mkdir(public_path($path), 0755, true);
        }
        if (!$file){
            return null;
        }
        $fileName = Str::random(10).'_'.Str::snake($file->getClientOriginalName());
        $file->move(public_path($path),$fileName);

        return "/$path/$fileName";
    }
}
