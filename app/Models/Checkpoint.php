<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Checkpoint extends Model
{
    protected $fillable = ['name', 'fullAddress', 'arrivalDate', 'departureDate', 'ggMapUri', 'lat', 'lng', 'route_id', 'form_id', 'user_groups_id'];

    public function route()
    {
        return $this->belongsTo(Route::class);
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function group()
    {
        return $this->belongsTo(UserGroup::class, 'user_groups_id');
    }

    public function checkins()
    {
        return $this->hasMany(Checkin::class);
    }

    public function answeredForms()
    {
        return $this->hasMany(AnsweredForm::class);
    }

    public function scopeSearchName($query, $name)
    {
        return $query->where('name', 'like', '%' . $name . '%');
    }

    public function scopeSearchFullAddress($query, $fullAddress)
    {
        return $query->where('fullAddress', 'like', '%' . $fullAddress . '%');
    }

    public function scopeSearchRouteName($query, $routeName)
    {
        return $query->whereHas('route', function($query) use($routeName){
            $query
                ->where('name', 'like', '%' . $routeName. '%');
        });
    }
}
