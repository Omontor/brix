<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $fillable = ['mode', 'title', 'description'];

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function checkpoint()
    {
        return $this->hasOne(Checkpoint::class);
    }

    public function cloneToForm()
    {
        try {
            $newForm = Form::create([
                'mode' => 'FORM',
                'title' => $this->title,
                'description' => $this->description
            ]);
            
            $questionsLyout = $this->questions;
            
            $questionsLyout->each(function ($qLayout, $key) use($newForm){
                $question = Question::create([
                    'name'          => $qLayout->name,
                    'questionType'  => $qLayout->questionType,
                    'order'         => $key + 1,
                    'form_id'       => $newForm->id
                ]);
                if ($question->isSelectable()) {
                    $optionsLayout = $qLayout->options;
                    $optionsLayout->each(function($oLayout, $key) use ($question){
                        if (!empty($oLayout->name)) {
                            $question->options()->create([
                                'name' => $oLayout->name
                            ]);
                        }
                    });
                }
            });
    
            return $newForm->refresh();
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
