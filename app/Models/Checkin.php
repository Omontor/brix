<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Checkin extends Model
{
    protected $fillable = ['status','user_id', 'checkpoint_id'];

    public function operator()
    {
        return $this->belongsTo(User::class);
    }

    public function checkpoint()
    {
        return $this->belongsTo(Checkpoint::class);
    }

    public static function calcStatus($arrivalDate,$departureDate)
    {
        $status = 'DONE';
        $now = Carbon::now('America/Mexico_City');
        $arrival = Carbon::parse($arrivalDate,'America/Mexico_City');
        $departure = Carbon::parse($departureDate,'America/Mexico_City');
        
        $earlyArrival = Carbon::parse($arrival,'America/Mexico_City')->subMinutes(20);
        $departureDateTenMinutes = Carbon::parse($departure,'America/Mexico_City')->addMinutes(10);
        
        /*
         * Si pasa la hora el sistema le pone retardo, 10 minutos más le pone falta
         * */
        if ($now->lessThan($earlyArrival)){
            throw new \Exception('EARLY_CHECKIN');
        }
        elseif ($now->greaterThanOrEqualTo($earlyArrival) && $now->lessThanOrEqualTo($departure)){
            $status = 'DONE';
        } elseif ($now->greaterThanOrEqualTo($earlyArrival) && $now->lessThanOrEqualTo($departureDateTenMinutes)){
            $status = 'DELAY';
        } elseif ($now->greaterThan($departureDateTenMinutes)){
            $status = 'LACK';
        }

        return $status;
    }
}
