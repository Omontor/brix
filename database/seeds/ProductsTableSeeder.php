<?php

use App\Models\Client;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = Client::all();

        $clients->each(function($client,$key){
           factory(Product::class, 2)->create([
               'client_id' => $client->id
           ]);
        });

    }
}
