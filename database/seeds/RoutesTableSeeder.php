<?php

use App\Models\Product;
use App\Models\Route;
use Illuminate\Database\Seeder;

class RoutesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = Product::all();

        $products->each(function ($product, $key){
            factory(Route::class,2)->create([
                'product_id' => $product->id
            ]);
        });
    }
}
