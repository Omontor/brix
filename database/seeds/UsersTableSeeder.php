<?php

use App\Models\Client;
use App\Models\UserGroup;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param Faker $faker
     * @return void
     */
    public function run(Faker $faker)
    {
        $groups = UserGroup::all();
        $mark = User::create([
            'name' => 'Administrador',
            'email' => 'admin@bbco.com',
            'email_verified_at' => now(),
            'contact' => '5555555555',
            'password' => Hash::make('secret')
        ]);
        $oliver = User::create([
            'name' => 'Oliver',
            'email' => 'oliver@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret')
        ]);
        $mark->assignRole('admin');
        $oliver->assignRole('admin');

        factory(User::class, 1)->create([
            'user_groups_id' => $faker->randomElement($groups->toArray())['id']
        ])->each(function ($user) use($faker, $groups) {
            $user->assignRole('operator');
        });
        factory(User::class, 1)->create([
            'client_id' => function(){
                return factory(Client::class)->create()->id;
            }
        ])->each(function ($user) use($faker) {
            $user->assignRole('employee');
        });
    }
}
