<?php

use App\Models\Checkpoint;
use App\Models\Form;
use App\Models\Route;
use App\Models\UserGroup;
use Illuminate\Database\Seeder;

class CheckpointsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $routes = Route::all();

        $routes->each(function($route, $key){
            factory(Checkpoint::class, 2)->create([
                'route_id' => $route->id,
                'form_id' => function () {
                    return factory(Form::class)->create(['mode' => 'FORM'])->id;
                },
                'user_groups_id' => function () {
                    return factory(UserGroup::class)->create()->id;
                },
            ]);
        });
    }
}
