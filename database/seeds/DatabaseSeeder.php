<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(GroupSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ClientsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(RoutesTableSeeder::class);
        $this->call(CheckpointsTableSeeder::class);
    }
}
