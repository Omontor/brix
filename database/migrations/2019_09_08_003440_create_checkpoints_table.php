<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkpoints', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->dateTime('startDate');
            $table->dateTime('finishDate');
            $table->unsignedBigInteger('route_id');
            $table->unsignedBigInteger('form_id');
            $table->unsignedBigInteger('user_groups_id'); 

            $table->foreign('user_groups_id')->references('id')->on('user_groups');
            $table->foreign('route_id')->references('id')->on('routes');
            $table->foreign('form_id')->references('id')->on('forms');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkpoints');
    }
}
