<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->mediumText('contact')->nullable();
            $table->boolean('isActive')->default(true);

            $table->unsignedBigInteger('client_id')->nullable(); //employee belongs to client
            $table->foreign('client_id')->references('id')->on('clients');

            $table->unsignedBigInteger('user_groups_id')->nullable(); 
            $table->foreign('user_groups_id')->references('id')->on('user_groups');
            
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
