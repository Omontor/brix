<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImagesToCheckpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('checkpoints', function (Blueprint $table) {
            
            $table->string('youtubeURL')->nullable();
            $table->string('localvideo')->nullable();
            $table->string('periscopeURL')->nullable();
            $table->integer('isstreaming')->default(0);
            $table->string('imageURL')->default('/images/defaults.clientdefault.png');
            $table->bigInteger('presupuestoevento')->default(0);
            $table->bigInteger('impactos')->default(0);
            $table->bigInteger('costoimpactos')->default(0);
            $table->bigInteger('directos')->default(0);
            $table->bigInteger('costodirectos')->default(0);
            $table->bigInteger('indirectos')->default(0);
            $table->bigInteger('costoindirectos')->default(0);   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('checkpoints', function (Blueprint $table) {
            //
        });
    }
}
