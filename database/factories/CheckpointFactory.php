<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Checkpoint;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Checkpoint::class, function (Faker $faker) {
    return [
        'name' => $faker->bs,
        'startDate' => $faker->dateTimeBetween('now', '+1 week'),
        'finishDate' => $faker->dateTimeBetween('now', '+1 week')
    ];
});
