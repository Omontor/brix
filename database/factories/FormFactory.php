<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Form;
use Faker\Generator as Faker;

$factory->define(Form::class, function (Faker $faker) {
    return [
        'mode' => $faker->randomElement(['LAYOUT', 'FORM']),
        'title' => $faker->realText(50),
        'description' => $faker->realText(400)
    ];
});
