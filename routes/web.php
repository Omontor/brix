<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/view', function (){
    return view('admin.users');
});
Route::get('/products/view', function (){
    return view('admin.products');
});
Route::get('/forms/create/view', function (){
    return view('admin.newForm');
});
Route::get('/clients/view', function (){
    return view('admin.clients');
});
Route::get('/groups/view', function (){
    return view('admin.groups');
});

Route::get('me/products/view', function (){
    return view('employee.products');
});
Route::get('/checkpoints/view', function (){
    return view('admin.checkpoints');
});
Route::get('/checkpoints/{id}/edit-view', function (){
    return view('admin.editCheckpoint');
});
Route::get('/checkpoints/create/view', function (){
    return view('admin.newCheckpoint');
});

Route::get('op/checkpoints/view', function (){
    return view('operator.checkpoints');
});


Route::get('/admin/createusers', function (){
    return view('admin.createusers');
});






Route::get('login/{socialNetwork}' , 'SocialLoginController@redirectToSocialNetwork')->name('login.social');
Route::get('login/{socialNetwork}/callback', 'SocialLoginController@handleSocialNetworkCallback');


// Controllers Within The "App\Http\Controllers\Client" Namespace
Route::namespace('Client')->group(function () {

Route::get('/client/social', 'SocialController@social')->name('client.social');
Route::get('/dashboard/view', 'DashboardController@dashboard');
    
    });


// Controllers Within The "App\Http\Controllers\Admin" Namespace
    Route::namespace('Admin')->group(function () {
    Route::resource('/users', 'UsersController');
    Route::resource('/clients/{clientId}/products', 'ClientProductsController');
    Route::resource('/clients/{clientId}/products/{productId}/routes', 'RoutesController');
    Route::resource('/products', 'ProductsController');
    Route::resource('/routes', 'RoutesController');
    Route::resource('/groups', 'UserGroupsController');
    Route::resource('/checkpoints/{checkpointId}/forms','FormController');
    Route::get('/dashboard/view', 'DashboardController@dashboard');

    Route::get('/client/{clientId}', 'ClientsController@show')->name('clients.show');
    Route::get('/product/{productId}', 'ProductsController@show')->name('products.show');
    Route::get('/clients/view', 'DashboardController@dashboard2')->name('admin.clients');
    Route::get('/products/view', 'DashboardController@productos')->name('admin.products');
    Route::get('/users/view', 'DashboardController@usuarios')->name('admin.users');
    Route::get('/groups/view', 'DashboardController@grupos')->name('admin.groups');
    Route::get('/checkpoints/view', 'DashboardController@checkpoints')->name('admin.checkpoints');
    Route::get('/venues/view', 'DashboardController@venues')->name('admin.venues');
    
    Route::get('/checkpoint/{checkpointId}', 'CheckpointsController@dashboard')->name('checkpoints.show');




});

Route::resource('forms', 'FormsController');
Route::resource('clients', 'ClientsController');
Route::resource('checkpoints', 'CheckpointsController');

// Controllers Within The "App\Http\Controllers\Customer" Namespace
Route::namespace('Employee')->group(function () {
    Route::resource('/me/products', 'ProductsController');
    Route::get('/me/products/{productId}/dashboard/view', 'DashboardController@dashboard');
});

// Controllers Within The "App\Http\Controllers\Customer" Namespace
Route::namespace('Operator')->group(function () {
    Route::resource('/op/checkpoints', 'CheckpointsController');
    Route::post('checkpoints/{checkpointId}/checkin', 'CheckpointsController@checkin');
});
