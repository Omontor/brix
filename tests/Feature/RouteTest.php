<?php

namespace Tests\Feature;

use App\Models\Client;
use App\Models\Product;
use App\Models\Route;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RouteTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_route()
    {
        $client = factory(Client::class)->create();
        $product = factory(Product::class)->create([
            'client_id' => $client->id
        ]);

        $response = $this->post("clients/{$client->id}/products/{$product->id}/routes",[
            "name" => "Walmart CDMX"
        ]);
        $response->assertStatus(201);
        $response->assertHeader('location');
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
            ]
        ]);
        $this->assertCount(1, Route::all());
    }

    public function can_get_routes_of_product()
    {
        $client = factory(Client::class)->create();
        $product = factory(Product::class)->create([
            'client_id' => $client->id
        ]);
        factory(Product::class,3)->create([
            'client_id' => $client->id
        ]);

        $response = $this->get("/clients/{$client->id}/products/$product->id/routes");

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                [
                    'id', 'name'
                ]
            ]
        ]);
    }
}
