<?php

namespace Tests\Feature;

use App\Models\Product;
use App\User;
use App\Models\Client;
use Spatie\Permission\Models\Role;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_be_created()
    {
        $roles = config('roles');
        foreach ($roles as $role){
            Role::create(['name' => $role]);
        }
        $response = $this->post('/users', [
            'name' => 'Mark APF',
            'email' => 'ferefuc@gmail.com',
            'password' => 'secret',
            'contact' => '545678987',
            'role' => 'admin'
        ]);
        $response->assertStatus(201);
        $response->assertHeader('location');
        $this->assertCount(1, User::all());
    }

    public function employees_can_be_created()
    {
        $roles = config('roles');
        foreach ($roles as $role){
            Role::create(['name' => $role]);
        }
        $client = factory(Client::class, 1)->create();
        $product = factory(Product::class, 1)->create();

        $response = $this->post('/users', [
            'name' => 'Employee 1',
            'email' => 'employee@gmail.com',
            'password' => 'secret',
            'contact' => '32454653',
            'role' => 'employee',
            'client_id' => $client->id,
            'product_id' => $product->id
        ]);

        $response->assertStatus(201);
        $response->assertHeader('location');
        $this->assertCount(1, User::all());
    }

    /** @test */
    /**public function name_email_password_required_in_creation()
    {
        $response = $this->post('/users', [
            'contact' => '545678987'
        ]);

        $response->assertJsonValidationErrors(['name','password','email']);
    }**/
}
