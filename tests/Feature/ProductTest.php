<?php

namespace Tests\Feature;

use App\Models\Client;
use App\Models\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_get_products_by_customer()
    {
        $client = factory(Client::class)->create();

        factory(Product::class,3)->create([
            'client_id' => $client->id
        ]);

        $response = $this->get("/clients/{$client->id}/products");

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data'=>[
                [
                    'id','name','code','isActive'
                ]
            ]
        ]);
    }

    /** @test */
    public function can_get_products()
    {
        $client = factory(Client::class)->create();
        factory(Product::class,5)->create([
            'client_id' => $client->id
        ]);

        $response = $this->get("/products");
        $response->assertStatus(200);
        $response->assertJsonStructure([
            [
                'id','name','code','isActive'
            ]
        ]);
    }

    /** @test */
    public function can_create_product()
    {
        $client = factory(Client::class)->create();
        $response = $this->post("/clients/{$client->id}/products", [
            'name' => 'The Best',
            'code' => '12345',
            'isActive' => true,
        ]);
        $response->assertStatus(201);
        $response->assertHeader('location');
        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'code',
                'isActive',
                'client_id'
            ]
        ]);
        $this->assertCount(1, Product::all());
    }
}
