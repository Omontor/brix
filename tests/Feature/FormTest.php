<?php

namespace Tests\Feature;

use App\Models\Form;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FormTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function get_forms_test()
    {
        factory(Form::class, 5)->create();
        $response = $this->get("/forms");

        $response->assertStatus(200);
        $response->assertJsonStructure([
            [
                'id','mode','title','description'
            ]
        ]);
    }

    /** @test */
    public function get_forms_layout_test()
    {
        factory(Form::class, 3)->create([
            'mode' => 'LAYOUT'
        ]);
        factory(Form::class, 2)->create([
            'mode' => 'FORM'
        ]);

        $response = $this->get("/forms?mode=LAYOUT");

        $response->assertStatus(200);
        $response->assertJsonCount(3);
        $response->assertJsonStructure([
            [
                'id','mode','title','description'
            ]
        ]);
    }

    /** @test */
    public function get_forms_form_test()
    {
        factory(Form::class, 3)->create([
            'mode' => 'LAYOUT'
        ]);
        factory(Form::class, 1)->create([
            'mode' => 'FORM'
        ]);

        $response = $this->get("/forms?mode=FORM");

        $response->assertStatus(200);
        $response->assertJsonCount(1);
        $response->assertJsonStructure([
            [
                'id','mode','title','description'
            ]
        ]);
    }

}
